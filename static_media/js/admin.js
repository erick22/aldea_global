(function($){
	$(document).ready(function() 
	{
		valor0 = $('#id_monitoreo').val();
		if (valor0 == '') {
			$('#capacitacionestalleres_set-group').hide();
			$('#establecimientosaf_set-group').hide();
			$('#sobrevivencia_set-group').hide();
		}

		valor = $('#id_monitoreo').val();
		if (valor == 'Capacitaciones y talleres') {
			$('#capacitacionestalleres_set-group').show();
			$('#establecimientosaf_set-group').hide();
			$('#sobrevivencia_set-group').hide();
		} else if (valor == 'Establecimiento de SAF') {
			$('#capacitacionestalleres_set-group').hide();
			$('#establecimientosaf_set-group').show();
			$('#sobrevivencia_set-group').hide();
		} else if (valor == 'Sobrevivencia') {
			$('#capacitacionestalleres_set-group').hide();
			$('#establecimientosaf_set-group').hide();
			$('#sobrevivencia_set-group').show();
		} else {
			$('#capacitacionestalleres_set-group').hide();
			$('#establecimientosaf_set-group').hide();
			$('#sobrevivencia_set-group').hide();
		}

		$('#id_monitoreo').change(function(){

			valor = $('#id_monitoreo').val();
			if (valor == 'Capacitaciones y talleres') {
				$('#capacitacionestalleres_set-group').show();
				$('#establecimientosaf_set-group').hide();
				$('#sobrevivencia_set-group').hide();
			} else if (valor == 'Establecimiento de SAF') {
				$('#capacitacionestalleres_set-group').hide();
				$('#establecimientosaf_set-group').show();
				$('#sobrevivencia_set-group').hide();
			} else if (valor == 'Sobrevivencia') {
				$('#capacitacionestalleres_set-group').hide();
				$('#establecimientosaf_set-group').hide();
				$('#sobrevivencia_set-group').show();
			} else {
				$('#capacitacionestalleres_set-group').hide();
				$('#establecimientosaf_set-group').hide();
				$('#sobrevivencia_set-group').hide();
			}
		});

	});
})(jQuery || django.jQuery);