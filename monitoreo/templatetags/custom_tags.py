from django import template
register = template.Library()
from monitoreo.models import *
from django.db.models.functions import Coalesce
from django.db.models import Avg, Sum, F, Count, Q

@register.filter(name='convertir_ha')
def ha(value):
	try:
		x = value * 0.7
	except:
		x = 0
	
	return '%.2f' % x

@register.filter(name='dosis_planta')
def dosis(obj):
	balance = BalanceNutrientes1.objects.filter(cafetalesfinca__numeroaplicaciones__aplicaciones = obj).values_list('id_productor','fecha_muestreo__year')
	
	distancia_plantas = DiagnosticoProductivo1.objects.filter(id_productor = balance[0][0],fecha_recuento__year = balance[0][1]).aggregate(
								total = Coalesce(Avg('sublotes__estimacionmanzana__distancia_plantas'),0))['total']
	distancia_surco = DiagnosticoProductivo1.objects.filter(id_productor = balance[0][0],fecha_recuento__year = balance[0][1]).aggregate(
							total = Coalesce(Avg('sublotes__estimacionmanzana__distancia_surco'),0))['total']
	plantas = 10000 / (distancia_plantas * distancia_surco)
	try:
		cantidad = obj.cantidad / plantas
	except:
		cantidad = 0

	return '%.5f %s' % (cantidad,obj.tipo_aplicacion.unidad_medida)