from django.shortcuts import render
from .models import *
from .forms import *
from lugar.models import *
from catalogo.models import *
from django.contrib.gis.geos import GEOSGeometry
from django.http import HttpResponse, HttpResponseRedirect
from aldea_global.tasks import *
import json as simplejson
from django.db.models import Avg, Sum, F, Count, Q
from django.db.models.functions import Coalesce
from django.contrib.auth.decorators import login_required
import datetime
import collections

# Create your views here.
def index(request,template='index.html'):
	registros = ProductoresGeneral.objects.count()
	renovacion = Renovacion.objects.distinct('id_productor').count()
	mantenimiento = Mantenimiento.objects.distinct('id_productor').count()
	#tabla 1
	municipios = ProductoresGeneral.objects.values_list('comunidad__municipio__nombre',flat=True).distinct('comunidad__municipio')
	list = []
	m_mujeres_mant = 0
	m_hombres_mant = 0
	m_total_mant = 0
	m_mujeres_reno = 0
	m_hombres_reno = 0
	m_total_reno = 0
	m_total_mant_reno = 0
	for obj in municipios:
		#mantenimiento
		mujeres_mant = Mantenimiento.objects.filter(id_productor__latitud__isnull = False,id_productor__longitud__isnull = False,
										id_productor__comunidad__municipio__nombre = obj,id_productor__sexo = 'femenino').distinct('id_productor').count()

		hombres_mant = Mantenimiento.objects.filter(id_productor__latitud__isnull = False,id_productor__longitud__isnull = False,
										id_productor__comunidad__municipio__nombre = obj,id_productor__sexo = 'masculino').distinct('id_productor').count()


		total_mant = mujeres_mant + hombres_mant
		m_mujeres_mant += mujeres_mant
		m_hombres_mant += hombres_mant
		m_total_mant += total_mant

		#renovacion
		mujeres_reno = Renovacion.objects.filter(id_productor__latitud__isnull = False,id_productor__longitud__isnull = False,
										id_productor__comunidad__municipio__nombre = obj,id_productor__sexo = 'femenino').distinct('id_productor').count()
		hombres_reno = Renovacion.objects.filter(id_productor__latitud__isnull = False,id_productor__longitud__isnull = False,
										id_productor__comunidad__municipio__nombre = obj,id_productor__sexo = 'masculino').distinct('id_productor').count()

		total_reno = mujeres_reno + hombres_reno

		total_general = total_mant + total_reno
		m_mujeres_reno += mujeres_reno
		m_hombres_reno += hombres_reno
		m_total_reno += total_reno

		m_total_mant_reno += total_general
		
		if mujeres_mant == 0 and hombres_mant == 0 and mujeres_reno == 0 and hombres_reno == 0:
			pass
		else:
			list.append((obj,mujeres_mant,hombres_mant,total_mant,mujeres_reno,hombres_reno,total_reno,total_general))

	#tabla 2
	responsable_at = PersonalCampo.objects.all()
	list_at = []
	at_mujeres_mant = 0
	at_hombres_mant = 0
	at_total_mant = 0
	at_mujeres_reno = 0
	at_hombres_reno = 0
	at_total_reno = 0
	at_total_mant_reno = 0

	for obj in responsable_at:
		#mantenimiento
		mujeres_mant = Mantenimiento.objects.filter(id_productor__latitud__isnull = False,id_productor__longitud__isnull = False,
										responsable_at = obj,id_productor__sexo = 'femenino').distinct('id_productor').count()

		hombres_mant = Mantenimiento.objects.filter(id_productor__latitud__isnull = False,id_productor__longitud__isnull = False,
										responsable_at = obj,id_productor__sexo = 'masculino').distinct('id_productor').count()

		
		total_mant = mujeres_mant + hombres_mant
		at_mujeres_mant += mujeres_mant
		at_hombres_mant += hombres_mant
		at_total_mant += total_mant

		#renovacion
		mujeres_reno = Renovacion.objects.filter(id_productor__latitud__isnull = False,id_productor__longitud__isnull = False,
										responsable_at = obj,id_productor__sexo = 'femenino').distinct('id_productor').count()

		hombres_reno = Renovacion.objects.filter(id_productor__latitud__isnull = False,id_productor__longitud__isnull = False,
										responsable_at = obj,id_productor__sexo = 'masculino').distinct('id_productor').count()

		total_reno = mujeres_reno + hombres_reno

		total_general = total_mant + total_reno
		at_mujeres_reno += mujeres_reno
		at_hombres_reno += hombres_reno
		at_total_reno += total_reno

		at_total_mant_reno += total_general
		
		if mujeres_mant == 0 and hombres_mant == 0 and mujeres_reno == 0 and hombres_reno == 0:
			pass
		else:
			list_at.append((obj.nombre_tecnico,mujeres_mant,hombres_mant,total_mant,mujeres_reno,hombres_reno,total_reno,total_general))

	#mapa
	
	prods = ProductoresGeneral.objects.filter(latitud__isnull = False, longitud__isnull = False)
	list_prod = []
	for x in prods:
		mant = Mantenimiento.objects.filter(id_productor = x.id).aggregate(total = Coalesce(Sum('area_mz'),0))['total']

		reno = Renovacion.objects.filter(id_productor = x.id).aggregate(total = Coalesce(Sum('area_mz'),0))['total']

		area_mz = mant + reno
		area_ha = (mant + reno) * 0.7

		list_prod.append((x.latitud,x.longitud,x.nombre,x.comunidad.nombre,area_mz,area_ha))

	return render(request, template, locals())

def _queryset_filtrado(request):
	params = {}

	if request.session['departamento']:
		if not request.session['municipio']:
			municipios = Municipio.objects.filter(departamento__in=request.session['departamento'])
			params['comunidad__municipio__in'] = municipios
		else:
			if request.session['comunidad']:
				params['comunidad__in'] = request.session['comunidad']
			else:
				params['comunidad__municipio__in'] = request.session['municipio']

	if request.session['sexo']:
		params['sexo'] = request.session['sexo']

	if request.session['estado']:
		params['estado'] = request.session['estado']

	# if request.session['tipo']:
	# 	params['tipo'] = request.session['tipo']

	unvalid_keys = []
	for key in params:
		if not params[key]:
			unvalid_keys.append(key)

	for key in unvalid_keys:
		del params[key]

	return ProductoresGeneral.objects.filter(**params)

def consulta(request,template = 'filtros.html'):
	if request.method == 'POST':
		form = FiltrosProductor(request.POST)
		if form.is_valid():
			request.session['departamento'] = form.cleaned_data['departamento']
			request.session['municipio'] = form.cleaned_data['municipio']
			request.session['comunidad'] = form.cleaned_data['comunidad']
			request.session['sexo'] = form.cleaned_data['sexo']
			request.session['estado'] = form.cleaned_data['estado']
			# request.session['tipo'] = form.cleaned_data['tipo']

			filtro = 1
		else:
			filtro = 0
	else:
		form = FiltrosProductor()
		try:
			del request.session['departamento']
			del request.session['municipio']
			del request.session['comunidad']
			del request.session['sexo']
			del request.session['estado']
			# del request.session['tipo']
		except:
			pass

	return render(request, template, locals())

def renovacion(request,template = 'renovacion.html'):
	filtro = _queryset_filtrado(request)
	
	if request.method == 'POST':
		form = Subfiltros(request.POST)
		if form.is_valid():

			inicio = form.cleaned_data['inicio']
			fin = form.cleaned_data['fin']
			tecnico = form.cleaned_data['tecnicos']

			params = {}
			params2 = {}
			if inicio and fin:
				params['renovacion__fecha_entrega__range'] = (inicio,fin)
				params2['asistenciatecnica__fecha_visita__range'] = (inicio,fin)
			if tecnico:
				params['renovacion__responsable_at'] = tecnico
				params2['asistenciatecnica__id_tecnico'] = tecnico


			productores = filtro.filter(**params).distinct('renovacion__id_productor').count()
			area_mz_total = filtro.filter(**params).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total']
			area_ha_total = filtro.filter(**params).aggregate(total = Coalesce(Sum('renovacion__area_mz') * 0.7,0))['total']

			lista_forestal = []
			total_forestales = filtro.filter(renovacion__plantasrenovacion__especie__tipo = 'Forestal',**params).aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
			forestales = filtro.filter(renovacion__plantasrenovacion__especie__tipo = 'Forestal',**params).values_list('renovacion__plantasrenovacion__especie__nombre',flat=True).distinct('renovacion__plantasrenovacion__especie')

			for x in forestales:
				cantidad = filtro.filter(renovacion__plantasrenovacion__especie__nombre = x,**params).aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
				lista_forestal.append((x,cantidad,saca_porcentajes(cantidad,total_forestales,False)))


			lista_frutales = []
			frutales = filtro.filter(renovacion__plantasrenovacion__especie__tipo = 'Frutal',**params).values_list('renovacion__plantasrenovacion__especie__nombre',flat=True).distinct('renovacion__plantasrenovacion__especie')
			total_frutales = filtro.filter(renovacion__plantasrenovacion__especie__tipo = 'Frutal',**params).aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
			for x in frutales:
				cantidad = filtro.filter(renovacion__plantasrenovacion__especie__nombre = x,**params).aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
				lista_frutales.append((x,cantidad,saca_porcentajes(cantidad,total_frutales,False)))

			#area mz rangos
			dict_mz = {}
			dict_mz['< 1'] = (filtro.filter(renovacion__area_mz__lt = 1,**params).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total'],
								filtro.filter(renovacion__area_mz__lt = 1,**params).count())

			dict_mz['1-2'] = (filtro.filter(renovacion__area_mz__gte = 1,renovacion__area_mz__lt = 2,**params).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total'],
								filtro.filter(renovacion__area_mz__gte = 1,renovacion__area_mz__lt = 2,**params).count())

			dict_mz['2-3'] = (filtro.filter(renovacion__area_mz__gte = 2,renovacion__area_mz__lt = 3,**params).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total'],
								filtro.filter(renovacion__area_mz__gte = 2,renovacion__area_mz__lt = 3,**params).count())

			dict_mz['3-4'] = (filtro.filter(renovacion__area_mz__gte = 3,renovacion__area_mz__lt = 4,**params).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total'],
								filtro.filter(renovacion__area_mz__gte = 3,renovacion__area_mz__lt = 4,**params).count())

			dict_mz['> 4'] = (filtro.filter(renovacion__area_mz__gte = 4,**params).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total'],
								filtro.filter(renovacion__area_mz__gte = 4,**params).count())

			#area mz rangos
			dict_ha = {}
			dict_ha['< 1'] = (filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__lt = 1,**params).aggregate(total = Coalesce(Sum(F('renovacion__area_mz') * 0.7),0))['total'],
								filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__lt = 1,**params).count())

			dict_ha['1-2'] = (filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 1,ha__lt = 2,**params).aggregate(total = Coalesce(Sum(F('renovacion__area_mz') * 0.7),0))['total'],
								filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 1,ha__lt = 2,**params).count())

			dict_ha['2-3'] = (filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 2,ha__lt = 3,**params).aggregate(total = Coalesce(Sum(F('renovacion__area_mz') * 0.7),0))['total'],
								filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 2,ha__lt = 3,**params).count())

			dict_ha['3-4'] = (filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 3,ha__lt = 4,**params).aggregate(total = Coalesce(Sum(F('renovacion__area_mz') * 0.7),0))['total'],
								filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 3,ha__lt = 4,**params).count())

			dict_ha['> 4'] = (filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 4,**params).aggregate(total = Coalesce(Sum(F('renovacion__area_mz') * 0.7),0))['total'],
								filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 4,**params).count())


			#tabla expedientes
			general_asociados = 0
			general_cedula = 0
			general_acta = 0
			general_mapa = 0
			general_visita = 0
			general_requisa = 0
			general_anexos = 0
			expedientes = []
			tecnicos = filtro.filter(renovacion__id_productor__in = filtro,**params).values_list('renovacion__responsable_at__nombre_tecnico',flat=True).distinct('renovacion__responsable_at')
			prods_acta = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),documentos__tipo_archivo = 'Acta de Intensión').values_list('nombre',flat=True)
			prods_anexos = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),documentos__tipo_archivo = 'Foto').values_list('nombre',flat=True)
			prods_mapa = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),subirpoligono__archivo__isnull = False).values_list('nombre',flat=True)
			
			for x in tecnicos:
				total_asociados = filtro.filter(renovacion__responsable_at__nombre_tecnico = x,**params).distinct('renovacion__id_productor').count()
				cedula = filtro.filter(renovacion__responsable_at__nombre_tecnico = x,renovacion__id_productor__cedula__isnull = False,**params).distinct('renovacion__id_productor').count()
				porcentaje_cedula = saca_porcentajes(cedula,total_asociados,False)
				
				acta = filtro.filter(renovacion__responsable_at__nombre_tecnico = x,renovacion__id_productor__nombre__in = prods_acta,**params).distinct('renovacion__id_productor').count()
				porcentaje_acta = saca_porcentajes(acta,total_asociados,False)

				mapa = filtro.filter(renovacion__responsable_at__nombre_tecnico = x,renovacion__id_productor__nombre__in = prods_mapa,**params).distinct('renovacion__id_productor').count()
				porcentaje_mapa = saca_porcentajes(mapa,total_asociados,False)

				visita = filtro.filter(asistenciatecnica__id_tecnico__nombre_tecnico = x,asistenciatecnica__tipo_visita = 'Asistencia técnica',asistenciatecnica__tipo = 'Renovación').distinct('asistenciatecnica__id_productor').count()
				porcentaje_visita = saca_porcentajes(visita,total_asociados,False)

				requisa = filtro.filter(renovacion__responsable_at__nombre_tecnico = x,**params).distinct('renovacion__id_productor').count()
				porcentaje_requisa = saca_porcentajes(requisa,total_asociados,False)

				anexos = filtro.filter(renovacion__id_productor__nombre__in = prods_anexos,renovacion__responsable_at__nombre_tecnico = x,**params).distinct('renovacion__id_productor').count()
				porcentaje_anexos = saca_porcentajes(anexos,total_asociados,False)

				promedio = (float(porcentaje_cedula) + float(porcentaje_acta) + float(porcentaje_mapa) + float(porcentaje_visita) + float(porcentaje_requisa) + float(porcentaje_anexos)) / 6

				expedientes.append((x,total_asociados,cedula,porcentaje_cedula,acta,porcentaje_acta,mapa,porcentaje_mapa,visita,porcentaje_visita,
									requisa,porcentaje_requisa,anexos,porcentaje_anexos,promedio))

				#totales generales
				general_asociados += total_asociados
				general_cedula += cedula
				general_acta += acta
				general_mapa += mapa
				general_visita += visita
				general_requisa += requisa
				general_anexos += anexos
	else:
		form = Subfiltros()

		productores = filtro.distinct('renovacion__id_productor').count()
		print(productores)
		area_mz_total = filtro.aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total']
		area_ha_total = filtro.aggregate(total = Coalesce(Sum('renovacion__area_mz') * 0.7,0))['total']

		lista_forestal = []
		total_forestales = filtro.filter(renovacion__plantasrenovacion__especie__tipo = 'Forestal').aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
		forestales = filtro.filter(renovacion__plantasrenovacion__especie__tipo = 'Forestal').values_list('renovacion__plantasrenovacion__especie__nombre',flat=True).distinct('renovacion__plantasrenovacion__especie')

		for x in forestales:
			cantidad = filtro.filter(renovacion__plantasrenovacion__especie__nombre = x).aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
			lista_forestal.append((x,cantidad,saca_porcentajes(cantidad,total_forestales,False)))


		lista_frutales = []
		frutales = filtro.filter(renovacion__plantasrenovacion__especie__tipo = 'Frutal').values_list('renovacion__plantasrenovacion__especie__nombre',flat=True).distinct('renovacion__plantasrenovacion__especie')
		total_frutales = filtro.filter(renovacion__plantasrenovacion__especie__tipo = 'Frutal').aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
		for x in frutales:
			cantidad = filtro.filter(renovacion__plantasrenovacion__especie__nombre = x).aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
			lista_frutales.append((x,cantidad,saca_porcentajes(cantidad,total_frutales,False)))

		#area mz rangos
		dict_mz = {}
		dict_mz['< 1'] = (filtro.filter(renovacion__area_mz__lt = 1).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total'],filtro.filter(renovacion__area_mz__lt = 1).count())
		dict_mz['1-2'] = (filtro.filter(renovacion__area_mz__gte = 1,renovacion__area_mz__lt = 2).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total'],filtro.filter(renovacion__area_mz__gte = 1,renovacion__area_mz__lt = 2).count())
		dict_mz['2-3'] = (filtro.filter(renovacion__area_mz__gte = 2,renovacion__area_mz__lt = 3).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total'],filtro.filter(renovacion__area_mz__gte = 2,renovacion__area_mz__lt = 3).count())
		dict_mz['3-4'] = (filtro.filter(renovacion__area_mz__gte = 3,renovacion__area_mz__lt = 4).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total'],filtro.filter(renovacion__area_mz__gte = 3,renovacion__area_mz__lt = 4).count())
		dict_mz['> 4'] = (filtro.filter(renovacion__area_mz__gte = 4).aggregate(total = Coalesce(Sum('renovacion__area_mz'),0))['total'],filtro.filter(renovacion__area_mz__gte = 4).count())

		#area mz rangos
		dict_ha = {}
		dict_ha['< 1'] = (filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__lt = 1).aggregate(total = Coalesce(Sum(F('renovacion__area_mz') * 0.7),0))['total'],
							filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__lt = 1).count())

		dict_ha['1-2'] = (filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 1,ha__lt = 2).aggregate(total = Coalesce(Sum(F('renovacion__area_mz') * 0.7),0))['total'],
							filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 1,ha__lt = 2).count())

		dict_ha['2-3'] = (filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 2,ha__lt = 3).aggregate(total = Coalesce(Sum(F('renovacion__area_mz') * 0.7),0))['total'],
							filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 2,ha__lt = 3).count())

		dict_ha['3-4'] = (filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 3,ha__lt = 4).aggregate(total = Coalesce(Sum(F('renovacion__area_mz') * 0.7),0))['total'],
							filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 3,ha__lt = 4).count())

		dict_ha['> 4'] = (filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 4).aggregate(total = Coalesce(Sum(F('renovacion__area_mz') * 0.7),0))['total'],
							filtro.annotate(ha = F('renovacion__area_mz') * 0.7).filter(ha__gte = 4).count())


		#tabla expedientes
		general_asociados = 0
		general_cedula = 0
		general_acta = 0
		general_mapa = 0
		general_visita = 0
		general_requisa = 0
		general_anexos = 0
		expedientes = []
		tecnicos = Renovacion.objects.filter(id_productor__in = filtro).values_list('responsable_at__nombre_tecnico',flat=True).distinct('responsable_at')
		prods_acta = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),documentos__tipo_archivo = 'Acta de Intensión').values_list('nombre',flat=True)
		prods_anexos = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),documentos__tipo_archivo = 'Foto').values_list('nombre',flat=True)
		prods_mapa = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),subirpoligono__archivo__isnull = False).values_list('nombre',flat=True)
		
		for x in tecnicos:
			total_asociados = filtro.filter(renovacion__responsable_at__nombre_tecnico = x).distinct('renovacion__id_productor').count()
			cedula = filtro.filter(renovacion__responsable_at__nombre_tecnico = x,renovacion__id_productor__cedula__isnull = False).distinct('renovacion__id_productor').count()
			porcentaje_cedula = saca_porcentajes(cedula,total_asociados,False)
			
			acta = filtro.filter(renovacion__responsable_at__nombre_tecnico = x,renovacion__id_productor__nombre__in = prods_acta).distinct('renovacion__id_productor').count()
			porcentaje_acta = saca_porcentajes(acta,total_asociados,False)

			mapa = filtro.filter(renovacion__responsable_at__nombre_tecnico = x,renovacion__id_productor__nombre__in = prods_mapa).distinct('renovacion__id_productor').count()
			porcentaje_mapa = saca_porcentajes(mapa,total_asociados,False)

			visita = filtro.filter(asistenciatecnica__id_tecnico__nombre_tecnico = x,asistenciatecnica__tipo_visita = 'Asistencia técnica',asistenciatecnica__tipo = 'Renovación').distinct('asistenciatecnica__id_productor').count()
			porcentaje_visita = saca_porcentajes(visita,total_asociados,False)

			requisa = filtro.filter(renovacion__responsable_at__nombre_tecnico = x).distinct('renovacion__id_productor').count()
			porcentaje_requisa = saca_porcentajes(requisa,total_asociados,False)

			anexos = filtro.filter(renovacion__id_productor__nombre__in = prods_anexos,renovacion__responsable_at__nombre_tecnico = x).distinct('renovacion__id_productor').count()
			porcentaje_anexos = saca_porcentajes(anexos,total_asociados,False)


			promedio = (float(porcentaje_cedula) + float(porcentaje_acta) + float(porcentaje_mapa) + float(porcentaje_visita) + float(porcentaje_requisa) + float(porcentaje_anexos)) / 6

			expedientes.append((x,total_asociados,cedula,porcentaje_cedula,acta,porcentaje_acta,mapa,porcentaje_mapa,visita,porcentaje_visita,
								requisa,porcentaje_requisa,anexos,porcentaje_anexos,promedio))

			#totales generales
			general_asociados += total_asociados
			general_cedula += cedula
			general_acta += acta
			general_mapa += mapa
			general_visita += visita
			general_requisa += requisa
			general_anexos += anexos

	return render(request, template, locals())

def mantenimiento(request,template = 'mantenimiento.html'):
	filtro = _queryset_filtrado(request)

	if request.method == 'POST':
		form = Subfiltros(request.POST)
		if form.is_valid():

			inicio = form.cleaned_data['inicio']
			fin = form.cleaned_data['fin']
			tecnico = form.cleaned_data['tecnicos']

			params = {}
			params2 = {}
			if inicio and fin:
				params['mantenimiento__fecha_entrega__range'] = (inicio,fin)
				params2['asistenciatecnica__fecha_visita__range'] = (inicio,fin)
			if tecnico:
				params['mantenimiento__responsable_at'] = tecnico
				params2['asistenciatecnica__id_tecnico'] = tecnico


			productores = filtro.filter(**params).distinct('mantenimiento__id_productor').exclude(mantenimiento__id_productor = None).count()
			area_mz_total = filtro.filter(**params).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total']
			area_ha_total = filtro.filter(**params).aggregate(total = Coalesce(Sum('mantenimiento__area_mz') * 0.7,0))['total']

			lista_forestal = []
			total_forestales = filtro.filter(mantenimiento__plantasmantenimiento__especie__tipo = 'Forestal',**params).aggregate(total = Sum('mantenimiento__plantasmantenimiento__cantidad'))['total']
			forestales = filtro.filter(mantenimiento__plantasmantenimiento__especie__tipo = 'Forestal',**params).values_list('mantenimiento__plantasmantenimiento__especie__nombre',flat=True).distinct('mantenimiento__plantasmantenimiento__especie')

			for x in forestales:
				cantidad = filtro.filter(mantenimiento__plantasmantenimiento__especie__nombre = x,**params).aggregate(total = Sum('mantenimiento__plantasmantenimiento__cantidad'))['total']
				lista_forestal.append((x,cantidad,saca_porcentajes(cantidad,total_forestales,False)))


			lista_frutales = []
			frutales = filtro.filter(mantenimiento__plantasmantenimiento__especie__tipo = 'Frutal',**params).values_list('mantenimiento__plantasmantenimiento__especie__nombre',flat=True).distinct('mantenimiento__plantasmantenimiento__especie')
			total_frutales = filtro.filter(mantenimiento__plantasmantenimiento__especie__tipo = 'Frutal',**params).aggregate(total = Sum('mantenimiento__plantasmantenimiento__cantidad'))['total']
			for x in frutales:
				cantidad = filtro.filter(mantenimiento__plantasmantenimiento__especie__nombre = x,**params).aggregate(total = Sum('mantenimiento__plantasmantenimiento__cantidad'))['total']
				lista_frutales.append((x,cantidad,saca_porcentajes(cantidad,total_frutales,False)))

			#area mz rangos
			dict_mz = {}
			dict_mz['< 1'] = (filtro.filter(mantenimiento__area_mz__lt = 1,**params).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total'],
								filtro.filter(mantenimiento__area_mz__lt = 1,**params).count())

			dict_mz['1-2'] = (filtro.filter(mantenimiento__area_mz__gte = 1,mantenimiento__area_mz__lt = 2,**params).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total'],
								filtro.filter(mantenimiento__area_mz__gte = 1,mantenimiento__area_mz__lt = 2,**params).count())

			dict_mz['2-3'] = (filtro.filter(mantenimiento__area_mz__gte = 2,mantenimiento__area_mz__lt = 3,**params).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total'],
								filtro.filter(mantenimiento__area_mz__gte = 2,mantenimiento__area_mz__lt = 3,**params).count())

			dict_mz['3-4'] = (filtro.filter(mantenimiento__area_mz__gte = 3,mantenimiento__area_mz__lt = 4,**params).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total'],
								filtro.filter(mantenimiento__area_mz__gte = 3,mantenimiento__area_mz__lt = 4,**params).count())

			dict_mz['> 4'] = (filtro.filter(mantenimiento__area_mz__gte = 4,**params).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total'],
								filtro.filter(mantenimiento__area_mz__gte = 4,**params).count())

			#area mz rangos
			dict_ha = {}
			dict_ha['< 1'] = (filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__lt = 1,**params).aggregate(total = Coalesce(Sum(F('mantenimiento__area_mz') * 0.7),0))['total'],
								filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__lt = 1,**params).count())

			dict_ha['1-2'] = (filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 1,ha__lt = 2,**params).aggregate(total = Coalesce(Sum(F('mantenimiento__area_mz') * 0.7),0))['total'],
								filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 1,ha__lt = 2,**params).count())

			dict_ha['2-3'] = (filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 2,ha__lt = 3,**params).aggregate(total = Coalesce(Sum(F('mantenimiento__area_mz') * 0.7),0))['total'],
								filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 2,ha__lt = 3,**params).count())

			dict_ha['3-4'] = (filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 3,ha__lt = 4,**params).aggregate(total = Coalesce(Sum(F('mantenimiento__area_mz') * 0.7),0))['total'],
								filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 3,ha__lt = 4,**params).count())

			dict_ha['> 4'] = (filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 4,**params).aggregate(total = Coalesce(Sum(F('mantenimiento__area_mz') * 0.7),0))['total'],
								filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 4,**params).count())


			#tabla expedientes
			general_asociados = 0
			general_cedula = 0
			general_acta = 0
			general_mapa = 0
			general_visita = 0
			general_requisa = 0
			general_anexos = 0
			expedientes = []
			tecnicos = filtro.filter(mantenimiento__id_productor__in = filtro,**params).values_list('mantenimiento__responsable_at__nombre_tecnico',flat=True).distinct('mantenimiento__responsable_at')
			prods_acta = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),documentos__tipo_archivo = 'Acta de Intensión').values_list('nombre',flat=True)
			prods_anexos = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),documentos__tipo_archivo = 'Foto').values_list('nombre',flat=True)
			prods_mapa = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),subirpoligono__archivo__isnull = False).values_list('nombre',flat=True)
			
			for x in tecnicos:
				total_asociados = filtro.filter(mantenimiento__responsable_at__nombre_tecnico = x,**params).distinct('mantenimiento__id_productor').count()
				cedula = filtro.filter(mantenimiento__responsable_at__nombre_tecnico = x,mantenimiento__id_productor__cedula__isnull = False,**params).distinct('mantenimiento__id_productor').count()
				porcentaje_cedula = saca_porcentajes(cedula,total_asociados,False)
				
				acta = filtro.filter(mantenimiento__responsable_at__nombre_tecnico = x,mantenimiento__id_productor__nombre__in = prods_acta,**params).distinct('mantenimiento__id_productor').count()
				porcentaje_acta = saca_porcentajes(acta,total_asociados,False)

				mapa = filtro.filter(mantenimiento__responsable_at__nombre_tecnico = x,mantenimiento__id_productor__nombre__in = prods_mapa,**params).distinct('mantenimiento__id_productor').count()
				porcentaje_mapa = saca_porcentajes(mapa,total_asociados,False)

				visita = filtro.filter(asistenciatecnica__id_tecnico__nombre_tecnico = x,asistenciatecnica__tipo_visita = 'Asistencia técnica',asistenciatecnica__tipo = 'Mantenimiento').distinct('asistenciatecnica__id_productor').count()
				porcentaje_visita = saca_porcentajes(visita,total_asociados,False)

				requisa = filtro.filter(mantenimiento__responsable_at__nombre_tecnico = x,**params).distinct('mantenimiento__id_productor').count()
				porcentaje_requisa = saca_porcentajes(requisa,total_asociados,False)

				anexos = filtro.filter(mantenimiento__id_productor__nombre__in = prods_anexos,mantenimiento__responsable_at__nombre_tecnico = x,**params).distinct('mantenimiento__id_productor').count()
				porcentaje_anexos = saca_porcentajes(anexos,total_asociados,False)

				promedio = (float(porcentaje_cedula) + float(porcentaje_acta) + float(porcentaje_mapa) + float(porcentaje_visita) + float(porcentaje_requisa) + float(porcentaje_anexos)) / 6

				expedientes.append((x,total_asociados,cedula,porcentaje_cedula,acta,porcentaje_acta,mapa,porcentaje_mapa,visita,porcentaje_visita,
									requisa,porcentaje_requisa,anexos,porcentaje_anexos,promedio))


				#totales generales
				general_asociados += total_asociados
				general_cedula += cedula
				general_acta += acta
				general_mapa += mapa
				general_visita += visita
				general_requisa += requisa
				general_anexos += anexos

	else:
		form = Subfiltros()
		productores = Mantenimiento.objects.filter(id_productor__in = filtro).distinct('id_productor').count()
		area_mz_total = filtro.aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total']
		area_ha_total = filtro.aggregate(total = Coalesce(Sum('mantenimiento__area_mz') * 0.7,0))['total']


		lista_forestal = []
		total_forestales = filtro.filter(mantenimiento__plantasmantenimiento__especie__tipo = 'Forestal').aggregate(total = Sum('mantenimiento__plantasmantenimiento__cantidad'))['total']
		forestales = filtro.filter(mantenimiento__plantasmantenimiento__especie__tipo = 'Forestal').values_list('mantenimiento__plantasmantenimiento__especie__nombre',flat=True).distinct('mantenimiento__plantasmantenimiento__especie')

		for x in forestales:
			cantidad = filtro.filter(mantenimiento__plantasmantenimiento__especie__nombre = x).aggregate(total = Sum('mantenimiento__plantasmantenimiento__cantidad'))['total']
			lista_forestal.append((x,cantidad,saca_porcentajes(cantidad,total_forestales,False)))


		lista_frutales = []
		frutales = filtro.filter(mantenimiento__plantasmantenimiento__especie__tipo = 'Frutal').values_list('mantenimiento__plantasmantenimiento__especie__nombre',flat=True).distinct('mantenimiento__plantasmantenimiento__especie')
		total_frutales = filtro.filter(mantenimiento__plantasmantenimiento__especie__tipo = 'Frutal').aggregate(total = Sum('mantenimiento__plantasmantenimiento__cantidad'))['total']
		for x in frutales:
			cantidad = filtro.filter(mantenimiento__plantasmantenimiento__especie__nombre = x).aggregate(total = Sum('mantenimiento__plantasmantenimiento__cantidad'))['total']
			lista_frutales.append((x,cantidad,saca_porcentajes(cantidad,total_frutales,False)))

		#area mz rangos
		dict_mz = {}
		dict_mz['< 1'] = (filtro.filter(mantenimiento__area_mz__lt = 1).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total'],filtro.filter(mantenimiento__area_mz__lt = 1).count())
		dict_mz['1-2'] = (filtro.filter(mantenimiento__area_mz__gte = 1,mantenimiento__area_mz__lt = 2).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total'],filtro.filter(mantenimiento__area_mz__gte = 1,mantenimiento__area_mz__lt = 2).count())
		dict_mz['2-3'] = (filtro.filter(mantenimiento__area_mz__gte = 2,mantenimiento__area_mz__lt = 3).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total'],filtro.filter(mantenimiento__area_mz__gte = 2,mantenimiento__area_mz__lt = 3).count())
		dict_mz['3-4'] = (filtro.filter(mantenimiento__area_mz__gte = 3,mantenimiento__area_mz__lt = 4).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total'],filtro.filter(mantenimiento__area_mz__gte = 3,mantenimiento__area_mz__lt = 4).count())
		dict_mz['> 4'] = (filtro.filter(mantenimiento__area_mz__gte = 4).aggregate(total = Coalesce(Sum('mantenimiento__area_mz'),0))['total'],filtro.filter(mantenimiento__area_mz__gte = 4).count())

		#area mz rangos
		dict_ha = {}
		dict_ha['< 1'] = (filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__lt = 1).aggregate(total = Coalesce(Sum(F('mantenimiento__area_mz') * 0.7),0))['total'],
							filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__lt = 1).count())

		dict_ha['1-2'] = (filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 1,ha__lt = 2).aggregate(total = Coalesce(Sum(F('mantenimiento__area_mz') * 0.7),0))['total'],
							filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 1,ha__lt = 2).count())

		dict_ha['2-3'] = (filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 2,ha__lt = 3).aggregate(total = Coalesce(Sum(F('mantenimiento__area_mz') * 0.7),0))['total'],
							filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 2,ha__lt = 3).count())

		dict_ha['3-4'] = (filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 3,ha__lt = 4).aggregate(total = Coalesce(Sum(F('mantenimiento__area_mz') * 0.7),0))['total'],
							filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 3,ha__lt = 4).count())

		dict_ha['> 4'] = (filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 4).aggregate(total = Coalesce(Sum(F('mantenimiento__area_mz') * 0.7),0))['total'],
							filtro.annotate(ha = F('mantenimiento__area_mz') * 0.7).filter(ha__gte = 4).count())


		#tabla expedientes
		general_asociados = 0
		general_cedula = 0
		general_acta = 0
		general_mapa = 0
		general_visita = 0
		general_requisa = 0
		general_anexos = 0
		expedientes = []
		tecnicos = Mantenimiento.objects.filter(id_productor__in = filtro).values_list('responsable_at__nombre_tecnico',flat=True).distinct('responsable_at')
		prods_acta = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),documentos__tipo_archivo = 'Acta de Intensión').values_list('nombre',flat=True)
		prods_anexos = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),documentos__tipo_archivo = 'Foto').values_list('nombre',flat=True)
		prods_mapa = ProductoresGeneral.objects.filter(nombre__in = filtro.values('nombre'),subirpoligono__archivo__isnull = False).values_list('nombre',flat=True)
		
		for x in tecnicos:
			total_asociados = Mantenimiento.objects.filter(responsable_at__nombre_tecnico = x,id_productor__in = filtro).distinct('id_productor').count()
			cedula = Mantenimiento.objects.filter(responsable_at__nombre_tecnico = x,id_productor__cedula__isnull = False,id_productor__in = filtro).distinct('id_productor').count()
			porcentaje_cedula = saca_porcentajes(cedula,total_asociados,False)
			
			acta = Mantenimiento.objects.filter(responsable_at__nombre_tecnico = x,id_productor__nombre__in = prods_acta,id_productor__in = filtro).distinct('id_productor').count()
			porcentaje_acta = saca_porcentajes(acta,total_asociados,False)

			mapa = Mantenimiento.objects.filter(responsable_at__nombre_tecnico = x,id_productor__nombre__in = prods_mapa,id_productor__in = filtro).distinct('id_productor').count()
			porcentaje_mapa = saca_porcentajes(mapa,total_asociados,False)

			visita = AsistenciaTecnica.objects.filter(id_tecnico__nombre_tecnico = x,tipo_visita = 'Asistencia técnica',tipo = 'Mantenimiento',id_productor__in = filtro).distinct('id_productor').count()
			porcentaje_visita = saca_porcentajes(visita,total_asociados,False)

			requisa = Mantenimiento.objects.filter(responsable_at__nombre_tecnico = x).distinct('id_productor').count()
			porcentaje_requisa = saca_porcentajes(requisa,total_asociados,False)

			anexos = Mantenimiento.objects.filter(id_productor__nombre__in = prods_anexos,responsable_at__nombre_tecnico = x,id_productor__in = filtro).distinct('id_productor').count()
			porcentaje_anexos = saca_porcentajes(anexos,total_asociados,False)

			promedio = (float(porcentaje_cedula) + float(porcentaje_acta) + float(porcentaje_mapa) + float(porcentaje_visita) + float(porcentaje_requisa) + float(porcentaje_anexos)) / 6

			expedientes.append((x,total_asociados,cedula,porcentaje_cedula,acta,porcentaje_acta,mapa,porcentaje_mapa,visita,porcentaje_visita,
								requisa,porcentaje_requisa,anexos,porcentaje_anexos,promedio))

			#totales generales
			general_asociados += total_asociados
			general_cedula += cedula
			general_acta += acta
			general_mapa += mapa
			general_visita += visita
			general_requisa += requisa
			general_anexos += anexos

	return render(request, template, locals())

def inventario_forestal(request, template = 'inventario-forestal.html'):
	filtro = _queryset_filtrado(request)

	if request.method == 'POST':
		form = Subfiltros(request.POST)
		if form.is_valid():

			inicio = form.cleaned_data['inicio']
			fin = form.cleaned_data['fin']
			tecnico = form.cleaned_data['tecnicos']

			params = {}
			params2 = {}
			if inicio and fin:
				params['renovacion__fecha_entrega__range'] = (inicio,fin)
				params2['inventarioarboles1__fecha__range'] = (inicio,fin)
			if tecnico:
				params['renovacion__responsable_at'] = tecnico
				params2['inventarioarboles1__encuestador'] = tecnico

			#tabla responsable at
			tecnicos = filtro.filter(**params).values_list('renovacion__responsable_at__nombre_tecnico',flat=True).distinct('renovacion__responsable_at')
			lista_at = []
			lista_at_2 = []
			total_inventarios = 0
			total_mz = 0
			total_ha = 0
			total_plantas = 0
			total_sobre = 0
			total_porcentaje = 0
			for obj in tecnicos:
				inventarios = filtro.filter(renovacion__responsable_at__nombre_tecnico = obj,
										**params).count()
				area_mz = filtro.filter(renovacion__responsable_at__nombre_tecnico = obj,
										**params).aggregate(total = Sum('renovacion__area_mz'))['total']
				try:
					area_ha = 0.7 * area_mz
				except:
					area_ha = 0
				
				plantas_entregadas = filtro.filter(renovacion__responsable_at__nombre_tecnico = obj,
										**params).aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
				sobrevivencia = filtro.filter(inventarioarboles1__encuestador__nombre_tecnico = obj,
											**params2).aggregate(total = Coalesce(Sum('inventarioarboles1__inventario__sobrevivencia'),0))['total']

				lista_at.append((obj,inventarios,area_mz,area_ha,plantas_entregadas,sobrevivencia,saca_porcentajes(sobrevivencia,plantas_entregadas,False)))

				productores = filtro.filter(renovacion__responsable_at__nombre_tecnico = obj,
										**params).aggregate(
											hombres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'masculino'),distinct = Q('renovacion__id_productor')),
											mujeres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'femenino'),distinct = Q('renovacion__id_productor')),
											total = Count('renovacion__id_productor',distinct = Q('renovacion__id_productor')),
											area_mz_hombres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')),
											area_mz_mujeres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')),
											total_area_mz = Sum('renovacion__area_mz'),
											area_ha_masculino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')) * 0.7,
											area_ha_femenino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')) * 0.7,
											total_area_ha = Sum('renovacion__area_mz') * 0.7,
											)
				lista_at_2.append((obj,productores['hombres'],productores['mujeres'],productores['total'],
									productores['area_mz_hombres'],productores['area_mz_mujeres'],
									productores['total_area_mz'],productores['area_ha_masculino'],
									productores['area_ha_femenino'],productores['total_area_ha']))

				#totales
				total_inventarios += inventarios
				try:
					total_mz += area_mz
				except:
					pass
				
				total_ha += area_ha
				total_plantas += plantas_entregadas
				try:
					total_sobre += sobrevivencia
				except:
					pass
			total_porcentaje = saca_porcentajes(total_sobre,total_plantas,False)
			productores_total = filtro.filter(**params).aggregate(
											hombres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'masculino'),distinct = Q('renovacion__id_productor')),
											mujeres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'femenino'),distinct = Q('renovacion__id_productor')),
											total = Count('renovacion__id_productor',distinct = Q('renovacion__id_productor')),
											area_mz_hombres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')),
											area_mz_mujeres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')),
											total_area_mz = Sum('renovacion__area_mz'),
											area_ha_masculino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')) * 0.7,
											area_ha_femenino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')) * 0.7,
											total_area_ha = Sum('renovacion__area_mz') * 0.7,
											)

			#tabla municipio
			municipios = filtro.filter(**params).values_list('renovacion__id_productor__comunidad__municipio__nombre',flat=True).distinct('renovacion__id_productor__comunidad__municipio')
			lista_mun = []
			lista_mun_2 = []
			mun_total_inventarios = 0
			mun_total_mz = 0
			mun_total_ha = 0
			mun_total_plantas = 0
			mun_total_sobre = 0
			mun_total_porcentaje = 0
			for obj in municipios:
				mun_inventarios = filtro.filter(renovacion__id_productor__comunidad__municipio__nombre = obj,
										**params).count()
				mun_area_mz = filtro.filter(renovacion__id_productor__comunidad__municipio__nombre = obj,
										**params).aggregate(total = Sum('renovacion__area_mz'))['total']
				mun_area_ha = 0.7 * mun_area_mz
				mun_plantas_entregadas = filtro.filter(renovacion__id_productor__comunidad__municipio__nombre = obj,
										**params).aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
				mun_sobrevivencia = filtro.filter(inventarioarboles1__id_productor__comunidad__municipio__nombre = obj,
											**params2).aggregate(total = Coalesce(Sum('inventarioarboles1__inventario__sobrevivencia'),0))['total']

				lista_mun.append((obj,mun_inventarios,mun_area_mz,mun_area_ha,mun_plantas_entregadas,mun_sobrevivencia,saca_porcentajes(mun_sobrevivencia,mun_plantas_entregadas,False)))

				productores_mun = filtro.filter(renovacion__id_productor__comunidad__municipio__nombre = obj,
											**params).aggregate(
											hombres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'masculino'),distinct = Q('renovacion__id_productor')),
											mujeres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'femenino'),distinct = Q('renovacion__id_productor')),
											total = Count('renovacion__id_productor',distinct = Q('renovacion__id_productor')),
											area_mz_hombres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')),
											area_mz_mujeres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')),
											total_area_mz = Sum('renovacion__area_mz'),
											area_ha_masculino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')) * 0.7,
											area_ha_femenino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')) * 0.7,
											total_area_ha = Sum('renovacion__area_mz') * 0.7,
											)
				lista_mun_2.append((obj,productores_mun['hombres'],productores_mun['mujeres'],productores_mun['total'],
									productores_mun['area_mz_hombres'],productores_mun['area_mz_mujeres'],
									productores_mun['total_area_mz'],productores_mun['area_ha_masculino'],
									productores_mun['area_ha_femenino'],productores_mun['total_area_ha']))

				#totales
				mun_total_inventarios += mun_inventarios
				mun_total_mz += mun_area_mz
				mun_total_ha += mun_area_ha
				mun_total_plantas += mun_plantas_entregadas
				try:
					mun_total_sobre += mun_sobrevivencia
				except:
					pass

			mun_total_porcentaje = saca_porcentajes(mun_total_sobre,mun_total_plantas,False)
			productores_mun_total = filtro.filter(**params).aggregate(
											hombres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'masculino'),distinct = Q('renovacion__id_productor')),
											mujeres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'femenino'),distinct = Q('renovacion__id_productor')),
											total = Count('renovacion__id_productor',distinct = Q('renovacion__id_productor')),
											area_mz_hombres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')),
											area_mz_mujeres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')),
											total_area_mz = Sum('renovacion__area_mz'),
											area_ha_masculino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')) * 0.7,
											area_ha_femenino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')) * 0.7,
											total_area_ha = Sum('renovacion__area_mz') * 0.7,
											)

			#tabla consolidado especies
			especies = filtro.filter(**params2).values_list('inventarioarboles1__inventario__especie',flat=True).distinct(
								'inventarioarboles1__inventario__especie').exclude(
								inventarioarboles1__inventario__especie__nombre = None)

			list_especies = []
			arboles = 0
			ab = 0
			vol = 0
			proyecciones = []
			for esp in Especie.objects.filter(id__in = especies):
				num_arboles = filtro.filter(inventarioarboles1__inventario__especie = esp,**params2).aggregate(
										total = Sum('inventarioarboles1__inventario__sobrevivencia'))['total']

				area_basal = filtro.filter(inventarioarboles1__inventario__especie = esp,**params2).aggregate(
										total = Sum('inventarioarboles1__inventario__area_basal'))['total']

				volumen = filtro.filter(inventarioarboles1__inventario__especie = esp,**params2).aggregate(
										total = Sum('inventarioarboles1__inventario__volumen'))['total']

				arboles += num_arboles
				try:
					ab += area_basal
				except:
					pass

				try:
					vol += volumen
				except:
					pass

				list_especies.append((esp,num_arboles,area_basal,volumen))

				#proyecciones
				crecimiento_anual = 0.35
				prom_dap = filtro.filter(inventarioarboles1__inventario__especie = esp,**params2).aggregate(
										total = Coalesce(Avg('inventarioarboles1__inventario__dap'),0))['total']

				total = 0
				lista_proy = []
				for x in range(0,10):
					if x == 0:
						total = prom_dap + crecimiento_anual
					else:
						total = total + crecimiento_anual

					if prom_dap == 0:
						total = 0

					lista_proy.append(total)

				proyecciones.append((esp,prom_dap,crecimiento_anual,lista_proy))

			#rango de anios
			last_year = filtro.filter(**params2).values_list('inventarioarboles1__fecha__year',flat=True).distinct('inventarioarboles1__fecha__year').exclude(
										inventarioarboles1__fecha__year = None).order_by('inventarioarboles1__fecha__year').last()
			rango = range(1,11)



	else:
		form = Subfiltros()
		tecnicos = Renovacion.objects.values_list('responsable_at__nombre_tecnico',flat=True).distinct('responsable_at')
		prods = Renovacion.objects.values_list('id_productor',flat=True).distinct('id_productor')
		lista_at = []
		lista_at_2 = []
		total_inventarios = 0
		total_mz = 0
		total_ha = 0
		total_plantas = 0
		total_sobre = 0
		total_porcentaje = 0
		for obj in tecnicos:
			inventarios = filtro.filter(renovacion__responsable_at__nombre_tecnico = obj).count()
			area_mz = filtro.filter(renovacion__responsable_at__nombre_tecnico = obj).aggregate(total = Sum('renovacion__area_mz'))['total']
			try:
				area_ha = 0.7 * area_mz
			except:
				area_ha = 0
			
			plantas_entregadas = filtro.filter(renovacion__responsable_at__nombre_tecnico = obj).aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
			sobrevivencia = filtro.filter(inventarioarboles1__encuestador__nombre_tecnico = obj).aggregate(total = Coalesce(Sum('inventarioarboles1__inventario__sobrevivencia'),0))['total']

			lista_at.append((obj,inventarios,area_mz,area_ha,plantas_entregadas,sobrevivencia,saca_porcentajes(sobrevivencia,plantas_entregadas,False)))

			productores = filtro.filter(renovacion__responsable_at__nombre_tecnico = obj).aggregate(
										hombres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'masculino'),distinct = Q('renovacion__id_productor')),
										mujeres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'femenino'),distinct = Q('renovacion__id_productor')),
										total = Count('renovacion__id_productor',distinct = Q('renovacion__id_productor')),
										area_mz_hombres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')),
										area_mz_mujeres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')),
										total_area_mz = Sum('renovacion__area_mz'),
										area_ha_masculino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')) * 0.7,
										area_ha_femenino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')) * 0.7,
										total_area_ha = Sum('renovacion__area_mz') * 0.7,
										)
			lista_at_2.append((obj,productores['hombres'],productores['mujeres'],productores['total'],
								productores['area_mz_hombres'],productores['area_mz_mujeres'],
								productores['total_area_mz'],productores['area_ha_masculino'],
								productores['area_ha_femenino'],productores['total_area_ha']))

			#totales
			total_inventarios += inventarios
			try:
				total_mz += area_mz
			except:
				pass
			
			try:
				total_ha += area_ha
			except:
				pass
			

			try:
				total_plantas += plantas_entregadas
			except:
				pass
			
			try:
				total_sobre += sobrevivencia
			except:
				pass
			
		total_porcentaje = saca_porcentajes(total_sobre,total_plantas,False)
		productores_total = filtro.aggregate(
										hombres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'masculino'),distinct = Q('renovacion__id_productor')),
										mujeres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'femenino'),distinct = Q('renovacion__id_productor')),
										total = Count('renovacion__id_productor',distinct = Q('renovacion__id_productor')),
										area_mz_hombres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')),
										area_mz_mujeres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')),
										total_area_mz = Sum('renovacion__area_mz'),
										area_ha_masculino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')) * 0.7,
										area_ha_femenino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')) * 0.7,
										total_area_ha = Sum('renovacion__area_mz') * 0.7,
										)

		#tabla municipio
		municipios = Renovacion.objects.values_list('id_productor__comunidad__municipio__nombre',flat=True).distinct('id_productor__comunidad__municipio').exclude(id_productor__comunidad__municipio__nombre = None)
		lista_mun = []
		lista_mun_2 = []
		mun_total_inventarios = 0
		mun_total_mz = 0
		mun_total_ha = 0
		mun_total_plantas = 0
		mun_total_sobre = 0
		mun_total_porcentaje = 0
		for obj in municipios:
			mun_inventarios = filtro.filter(renovacion__id_productor__comunidad__municipio__nombre = obj).count()
			mun_area_mz = filtro.filter(renovacion__id_productor__comunidad__municipio__nombre = obj).aggregate(total = Sum('renovacion__area_mz'))['total']
			try:
				mun_area_ha = 0.7 * mun_area_mz
			except:
				mun_area_ha = 0
			
			mun_plantas_entregadas = filtro.filter(renovacion__id_productor__comunidad__municipio__nombre = obj).aggregate(total = Sum('renovacion__plantasrenovacion__cantidad'))['total']
			mun_sobrevivencia = filtro.filter(inventarioarboles1__id_productor__comunidad__municipio__nombre = obj).aggregate(total = Coalesce(Sum('inventarioarboles1__inventario__sobrevivencia'),0))['total']

			lista_mun.append((obj,mun_inventarios,mun_area_mz,mun_area_ha,mun_plantas_entregadas,mun_sobrevivencia,saca_porcentajes(mun_sobrevivencia,mun_plantas_entregadas,False)))

			productores_mun = filtro.filter(renovacion__id_productor__comunidad__municipio__nombre = obj).aggregate(
										hombres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'masculino'),distinct = Q('renovacion__id_productor')),
										mujeres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'femenino'),distinct = Q('renovacion__id_productor')),
										total = Count('renovacion__id_productor',distinct = Q('renovacion__id_productor')),
										area_mz_hombres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')),
										area_mz_mujeres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')),
										total_area_mz = Sum('renovacion__area_mz'),
										area_ha_masculino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')) * 0.7,
										area_ha_femenino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')) * 0.7,
										total_area_ha = Sum('renovacion__area_mz') * 0.7,
										)
			lista_mun_2.append((obj,productores_mun['hombres'],productores_mun['mujeres'],productores_mun['total'],
								productores_mun['area_mz_hombres'],productores_mun['area_mz_mujeres'],
								productores_mun['total_area_mz'],productores_mun['area_ha_masculino'],
								productores_mun['area_ha_femenino'],productores_mun['total_area_ha']))

			#totales
			mun_total_inventarios += mun_inventarios
			try:
				mun_total_mz += mun_area_mz
			except:
				pass
			
			try:
				mun_total_ha += mun_area_ha
			except:
				pass

			try:
				mun_total_plantas += mun_plantas_entregadas
			except:
				pass			
			
			try:
				mun_total_sobre += mun_sobrevivencia
			except:
				pass

		mun_total_porcentaje = saca_porcentajes(mun_total_sobre,mun_total_plantas,False)
		productores_mun_total = filtro.aggregate(
										hombres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'masculino'),distinct = Q('renovacion__id_productor')),
										mujeres = Count('renovacion__id_productor',filter = Q(renovacion__id_productor__sexo = 'femenino'),distinct = Q('renovacion__id_productor')),
										total = Count('renovacion__id_productor',distinct = Q('renovacion__id_productor')),
										area_mz_hombres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')),
										area_mz_mujeres = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')),
										total_area_mz = Sum('renovacion__area_mz'),
										area_ha_masculino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'masculino')) * 0.7,
										area_ha_femenino = Sum('renovacion__area_mz', filter = Q(renovacion__id_productor__sexo = 'femenino')) * 0.7,
										total_area_ha = Sum('renovacion__area_mz') * 0.7,
										)

		#tabla consolidado especies
		especies = filtro.values_list('inventarioarboles1__inventario__especie',flat=True).distinct(
								'inventarioarboles1__inventario__especie').exclude(
								inventarioarboles1__inventario__especie__nombre = None)

		list_especies = []
		arboles = 0
		ab = 0
		vol = 0
		proyecciones = []
		for esp in Especie.objects.filter(id__in = especies):
			num_arboles = filtro.filter(inventarioarboles1__inventario__especie = esp).aggregate(
									total = Sum('inventarioarboles1__inventario__sobrevivencia'))['total']

			area_basal = filtro.filter(inventarioarboles1__inventario__especie = esp).aggregate(
									total = Sum('inventarioarboles1__inventario__area_basal'))['total']

			volumen = filtro.filter(inventarioarboles1__inventario__especie = esp).aggregate(
									total = Sum('inventarioarboles1__inventario__volumen'))['total']

			arboles += num_arboles
			try:
				ab += area_basal
			except:
				pass

			try:
				vol += volumen
			except:
				pass
			
			

			list_especies.append((esp,num_arboles,area_basal,volumen))

			#proyecciones
			crecimiento_anual = 0.35
			prom_dap = filtro.filter(inventarioarboles1__inventario__especie = esp).aggregate(
									total = Coalesce(Avg('inventarioarboles1__inventario__dap'),0))['total']

			total = 0
			lista_proy = []
			for x in range(0,10):
				if x == 0:
					total = prom_dap + crecimiento_anual
				else:
					total = total + crecimiento_anual

				if prom_dap == 0:
					total = 0
 
				lista_proy.append(total)

			proyecciones.append((esp,prom_dap,crecimiento_anual,lista_proy))


		#rango de anios
		last_year = filtro.values_list('inventarioarboles1__fecha__year',flat=True).distinct('inventarioarboles1__fecha__year').exclude(
									inventarioarboles1__fecha__year = None).order_by('inventarioarboles1__fecha__year').last()
		rango = range(1,11)

		#area inventariada
		area_mz_inv = filtro.aggregate(total = Coalesce(Sum('renovacion__area_mz'),0) + Coalesce(Sum('mantenimiento__area_mz'),0))['total']

		try:
			area_ha_inv = area_mz_inv * 0.7
		except:
			area_ha_inv = 0

		#conteos
		total_forestales_reno = filtro.filter(renovacion__plantasrenovacion__especie__tipo = 'Forestal').aggregate(total = Coalesce(Sum('renovacion__plantasrenovacion__cantidad'),0))['total']
		total_forestales_mant = filtro.filter(mantenimiento__plantasmantenimiento__especie__tipo = 'Forestal').aggregate(total = Coalesce(Sum('mantenimiento__plantasmantenimiento__cantidad'),0))['total']
		total_forestales = total_forestales_reno + total_forestales_mant

		total_sobrevivencia = filtro.filter(inventarioarboles1__inventario__especie__tipo = 'Forestal').aggregate(total = Coalesce(Sum('inventarioarboles1__inventario__sobrevivencia'),0))['total']

		forestales = {}
		for esp in Especie.objects.filter(tipo = 'Forestal'):
			reno = filtro.filter(renovacion__plantasrenovacion__especie = esp).aggregate(total = Coalesce(Sum('renovacion__plantasrenovacion__cantidad'),0))['total']
			mant = filtro.filter(mantenimiento__plantasmantenimiento__especie = esp).aggregate(total = Coalesce(Sum('mantenimiento__plantasmantenimiento__cantidad'),0))['total']
			cantidad = reno + mant
			sobrevicencia = filtro.filter(inventarioarboles1__inventario__especie = esp).aggregate(total = Coalesce(Sum('inventarioarboles1__inventario__sobrevivencia'),0))['total']
			forestales[esp.nombre] = cantidad,sobrevicencia

	return render(request, template, locals())

def asistencia_tecnica(request, template = 'asistencia-tecnica.html'):
	filtro = _queryset_filtrado(request)

	if request.method == 'POST':
		form = Subfiltros(request.POST)
		if form.is_valid():

			inicio = form.cleaned_data['inicio']
			fin = form.cleaned_data['fin']
			tecnico = form.cleaned_data['tecnicos']

			params = {}
			if inicio and fin:
				params['asistenciatecnica__fecha_visita__range'] = (inicio,fin)
			if tecnico:
				params['asistenciatecnica__id_tecnico'] = tecnico

			tecnicos = filtro.filter(asistenciatecnica__tipo_visita = 'Asistencia técnica',**params).values_list('asistenciatecnica__id_tecnico__nombre_tecnico',flat=True).distinct('asistenciatecnica__id_tecnico')
			lista = []
			total_f_mant = 0
			total_m_mant = 0
			total_f_reno = 0
			total_m_reno = 0
			total_general_sum = 0
			for obj in tecnicos:
				femenino_mant = filtro.filter(asistenciatecnica__id_tecnico__nombre_tecnico = obj,asistenciatecnica__id_productor__sexo = 'femenino',
												tipo = 'Mantenimiento',asistenciatecnica__tipo_visita = 'Asistencia técnica',**params).count()

				masculino_mant = filtro.filter(asistenciatecnica__id_tecnico__nombre_tecnico = obj,asistenciatecnica__id_productor__sexo = 'masculino',
												tipo = 'Mantenimiento',asistenciatecnica__tipo_visita = 'Asistencia técnica',**params).count()

				femenino_reno = filtro.filter(asistenciatecnica__id_tecnico__nombre_tecnico = obj,asistenciatecnica__id_productor__sexo = 'femenino',
												tipo = 'Renovación',asistenciatecnica__tipo_visita = 'Asistencia técnica',**params).count()
				masculino_reno = filtro.filter(asistenciatecnica__id_tecnico__nombre_tecnico = obj,asistenciatecnica__id_productor__sexo = 'masculino',
												tipo = 'Renovación',asistenciatecnica__tipo_visita = 'Asistencia técnica',**params).count()

				total_general = femenino_mant + masculino_mant + femenino_reno + masculino_reno
				lista.append((obj,femenino_mant,masculino_mant,femenino_reno,masculino_reno,total_general))

				#totales tabla
				total_f_mant += femenino_mant 
				total_m_mant += masculino_mant 
				total_f_reno += femenino_reno 
				total_m_reno += masculino_reno
				total_general_sum += total_general

	else:
		form = Subfiltros()
		tecnicos = AsistenciaTecnica.objects.filter(tipo_visita = 'Asistencia técnica').values_list('id_tecnico__nombre_tecnico',flat=True).distinct('id_tecnico')
		lista = []
		total_f_mant = 0
		total_m_mant = 0
		total_f_reno = 0
		total_m_reno = 0
		total_general_sum = 0
		for obj in tecnicos:
			femenino_mant = AsistenciaTecnica.objects.filter(id_tecnico__nombre_tecnico = obj,id_productor__sexo = 'femenino',
											tipo = 'Mantenimiento',tipo_visita = 'Asistencia técnica',id_productor__in = filtro).count()
			masculino_mant = AsistenciaTecnica.objects.filter(id_tecnico__nombre_tecnico = obj,id_productor__sexo = 'masculino',
											tipo = 'Mantenimiento',tipo_visita = 'Asistencia técnica',id_productor__in = filtro).count()

			femenino_reno = AsistenciaTecnica.objects.filter(id_tecnico__nombre_tecnico = obj,id_productor__sexo = 'femenino',
											tipo = 'Renovación',tipo_visita = 'Asistencia técnica',id_productor__in = filtro).count()
			masculino_reno = AsistenciaTecnica.objects.filter(id_tecnico__nombre_tecnico = obj,id_productor__sexo = 'masculino',
											tipo = 'Renovación',tipo_visita = 'Asistencia técnica',id_productor__in = filtro).count()

			total_general = femenino_mant + masculino_mant + femenino_reno + masculino_reno
			lista.append((obj,femenino_mant,masculino_mant,femenino_reno,masculino_reno,total_general))

			#totales tabla
			total_f_mant += femenino_mant 
			total_m_mant += masculino_mant 
			total_f_reno += femenino_reno 
			total_m_reno += masculino_reno
			total_general_sum += total_general

	return render(request, template, locals())

def capacitaciones(request, template = 'capacitaciones.html'):
	filtro = _queryset_filtrado(request)

	if request.method == 'POST':
		form = Subfiltros(request.POST)
		if form.is_valid():

			inicio = form.cleaned_data['inicio']
			fin = form.cleaned_data['fin']
			tecnico = form.cleaned_data['tecnicos']

			params = {}
			params2 = {}
			if inicio and fin:
				params['capacitaciones__fecha_evento__range'] = (inicio,fin)
				params2['fecha_evento__range'] = (inicio,fin)
			if tecnico:
				params['capacitaciones__responsable_evento'] = tecnico
				params2['responsable_evento'] = tecnico

			dict_eventos = {}
			for x in CatalogoEventos.objects.all():
				count = filtro.filter(capacitaciones__codigo_evento = x,**params).distinct('capacitaciones__productores').count()
				dict_eventos[x.tema] = count

			evento = Capacitaciones.objects.filter(**params2).distinct('codigo_evento')
			lista = []
			total_femenino = 0
			total_masculino = 0
			total_general = 0

			lista_personas = []
			total_femenino_personas = 0
			total_masculino_personas = 0
			total_general_personas = 0
			for obj in evento:
				#tabla capacitaciones
				femenino = filtro.filter(**params,
									capacitaciones__codigo_evento = obj.codigo_evento,sexo = 'femenino').count()

				masculino = filtro.filter(**params,
									capacitaciones__codigo_evento = obj.codigo_evento,sexo = 'masculino').count()
				total = femenino + masculino
				lista.append((obj.codigo_evento.linea_presupuestaria,obj.codigo_evento.tema,femenino,masculino,total))

				total_femenino += femenino
				total_masculino += masculino
				total_general += total

				#tabla personas
				p_femenino = filtro.filter(**params,
									capacitaciones__codigo_evento = obj.codigo_evento,sexo = 'femenino').distinct('id').count()
				p_masculino = filtro.filter(**params,
									capacitaciones__codigo_evento = obj.codigo_evento,sexo = 'masculino').distinct('id').count()
				p_total = p_femenino + p_masculino
				lista_personas.append((obj.codigo_evento.linea_presupuestaria,obj.codigo_evento.tema,p_femenino,p_masculino,p_total))

				total_femenino_personas += p_femenino
				total_masculino_personas += p_masculino
				total_general_personas += p_total
	else:
		form = Subfiltros()
		dict_eventos = {}
		for x in CatalogoEventos.objects.all():
			count = Capacitaciones.objects.filter(codigo_evento = x,productores__in = filtro.distinct('capacitaciones__productores')).count()
			dict_eventos[x.tema] = count

		evento = Capacitaciones.objects.distinct('codigo_evento')
		lista = []
		total_femenino = 0
		total_masculino = 0
		total_general = 0

		lista_personas = []
		total_femenino_personas = 0
		total_masculino_personas = 0
		total_general_personas = 0
		for obj in evento:
			#tabla capacitaciones
			femenino = filtro.filter(capacitaciones__codigo_evento = obj.codigo_evento,sexo = 'femenino').count()

			masculino = filtro.filter(capacitaciones__codigo_evento = obj.codigo_evento,sexo = 'masculino').count()
			total = femenino + masculino
			lista.append((obj.codigo_evento.linea_presupuestaria,obj.codigo_evento.tema,femenino,masculino,total))

			total_femenino += femenino
			total_masculino += masculino
			total_general += total

			#tabla personas
			p_femenino = filtro.filter(capacitaciones__codigo_evento = obj.codigo_evento,sexo = 'femenino').distinct('id').count()
			p_masculino = filtro.filter(capacitaciones__codigo_evento = obj.codigo_evento,sexo = 'masculino').distinct('id').count()
			p_total = p_femenino + p_masculino
			lista_personas.append((obj.codigo_evento.linea_presupuestaria,obj.codigo_evento.tema,p_femenino,p_masculino,p_total))

			total_femenino_personas += p_femenino
			total_masculino_personas += p_masculino
			total_general_personas += p_total

	return render(request, template, locals())

def diagnostico_productivo(request,template = 'diagnostico-productivo.html'):
	filtro = _queryset_filtrado(request)

	if request.method == 'POST':
		form = Subfiltros(request.POST)
		if form.is_valid():

			inicio = form.cleaned_data['inicio']
			fin = form.cleaned_data['fin']
			tecnico = form.cleaned_data['tecnicos']

			params = {}
			params2 = {}
			if inicio and fin:
				params['diagnosticoproductivo1__fecha_recuento__range'] = (inicio,fin)
				params2['fecha_recuento__range'] = (inicio,fin)
			if tecnico:
				params['diagnosticoproductivo1__id_tecnico'] = tecnico
				params2['id_tecnico'] = tecnico

			#porcentajes
			plantas_prod = filtro.filter(**params).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_productiva'),0))['total']
			puntos = filtro.filter(**params).aggregate(total = Coalesce(Count('diagnosticoproductivo1__sublotes'),0))['total'] * 25
			total_cafetos = filtro.filter(**params).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__estimacionmanzana__total_cafetos'),0))['total']

			try:
				porcentaje_plantas_prod = (plantas_prod / puntos) * 100
			except:
				porcentaje_plantas_prod = 0

			plantas_poda = filtro.filter(**params).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_poda_ligera'),0))['total']
			
			try:
				porcentaje_plantas_poda_ligera = (plantas_poda / puntos) * total_cafetos
			except:
				porcentaje_plantas_poda_ligera = 0
			

			plantas_recepo = filtro.filter(**params).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_recepo'),0))['total']
			try:
				porcentaje_plantas_recepo = (plantas_recepo / puntos) * total_cafetos
			except:
				porcentaje_plantas_recepo = 0
			
			planta_requiere_resiembra = filtro.filter(**params).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_requiere_resiembra'),0))['total']
			try:
				porcentaje_planta_requiere_resiembra = (planta_requiere_resiembra / puntos) * total_cafetos
			except:
				porcentaje_planta_requiere_resiembra = 0

			planta_joven = filtro.filter(**params).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_joven'),0))['total']
			try:
				porcentaje_planta_joven = (planta_joven / puntos) * total_cafetos
			except:
				porcentaje_planta_joven = 0
			
			falla_fisica = filtro.filter(**params).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__falla_fisica'),0))['total']
			try:
				porcentaje_falla_fisica = (falla_fisica / puntos) * total_cafetos
			except:
				porcentaje_falla_fisica = 0
			

			porcentaje_sombra = filtro.filter(**params).aggregate(total = Coalesce(Avg('diagnosticoproductivo1__sublotes__nivelsombra__nivel_sombra'),0))['total']

			#tablas
			tecnicos_diagnostico = DiagnosticoProductivo1.objects.filter(**params2).values_list(
									'id_tecnico__nombre_tecnico').distinct('id_tecnico')
			lista = []
			total_femenino = 0
			total_masculino = 0
			total_general = 0

			b_lista = []
			b_total_femenino = 0
			b_total_masculino = 0
			b_total_general = 0

			for obj in tecnicos_diagnostico:
				if obj[0] != None:
					#diagnostico prod
					femenino = filtro.filter(**params,
												diagnosticoproductivo1__id_tecnico__nombre_tecnico = obj[0],
												diagnosticoproductivo1__id_productor__sexo = 'femenino').count()
					masculino = filtro.filter(**params,
												diagnosticoproductivo1__id_tecnico__nombre_tecnico = obj[0],
												diagnosticoproductivo1__id_productor__sexo = 'masculino').count()
					total = femenino + masculino

					lista.append((obj[0],femenino,masculino,total))

					total_femenino += femenino
					total_masculino += masculino
					total_general += total

	else:
		form = Subfiltros()
		#porcentajes
		plantas_prod = filtro.aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_productiva'),0))['total']
		puntos = filtro.aggregate(total = Coalesce(Count('diagnosticoproductivo1__sublotes'),0))['total'] * 25
		total_cafetos = filtro.aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__estimacionmanzana__total_cafetos'),0))['total']

		try:
			porcentaje_plantas_prod = (plantas_prod / puntos) * 100
			# print(porcentaje_plantas_prod,"%")
			# print('plantas',plantas_prod,'puntos',puntos,'cafetos',total_cafetos)
		except:
			porcentaje_plantas_prod = 0

		plantas_poda = filtro.aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_poda_ligera'),0))['total']
		
		try:
			porcentaje_plantas_poda_ligera = (plantas_poda / puntos) * total_cafetos
		except:
			porcentaje_plantas_poda_ligera = 0
		

		plantas_recepo = filtro.aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_recepo'),0))['total']
		try:
			porcentaje_plantas_recepo = (plantas_recepo / puntos) * total_cafetos
		except:
			porcentaje_plantas_recepo = 0
		
		planta_requiere_resiembra = filtro.aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_requiere_resiembra'),0))['total']
		try:
			porcentaje_planta_requiere_resiembra = (planta_requiere_resiembra / puntos) * total_cafetos
		except:
			porcentaje_planta_requiere_resiembra = 0

		planta_joven = filtro.aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_joven'),0))['total']
		try:
			porcentaje_planta_joven = (planta_joven / puntos) * total_cafetos
		except:
			porcentaje_planta_joven = 0
		
		falla_fisica = filtro.aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__falla_fisica'),0))['total']
		try:
			porcentaje_falla_fisica = (falla_fisica / puntos) * total_cafetos
		except:
			porcentaje_falla_fisica = 0

		porcentaje_sombra = filtro.aggregate(total = Coalesce(Avg('diagnosticoproductivo1__sublotes__nivelsombra__nivel_sombra'),0))['total']

		#tablas
		tecnicos_diagnostico = DiagnosticoProductivo1.objects.values_list(
								'id_tecnico__nombre_tecnico').distinct('id_tecnico')
		lista = []
		total_femenino = 0
		total_masculino = 0
		total_general = 0

		b_lista = []
		b_total_femenino = 0
		b_total_masculino = 0
		b_total_general = 0

		for obj in tecnicos_diagnostico:
			if obj[0] != None:
				#diagnostico prod
				femenino = filtro.filter(diagnosticoproductivo1__id_tecnico__nombre_tecnico = obj[0],
															diagnosticoproductivo1__id_productor__sexo = 'femenino').count()
				masculino = filtro.filter(diagnosticoproductivo1__id_tecnico__nombre_tecnico = obj[0],
															diagnosticoproductivo1__id_productor__sexo = 'masculino').count()
				total = femenino + masculino

				lista.append((obj[0],femenino,masculino,total))

				total_femenino += femenino
				total_masculino += masculino
				total_general += total
		
	return render(request, template, locals())

def balance_nutrientes(request,template = 'balance-nutrientes.html'):
	filtro = _queryset_filtrado(request)

	if request.method == 'POST':
		form = Subfiltros(request.POST)
		if form.is_valid():

			inicio = form.cleaned_data['inicio']
			fin = form.cleaned_data['fin']
			tecnico = form.cleaned_data['tecnicos']

			params = {}
			params2 = {}
			if inicio and fin:
				params['balancenutrientes1__fecha_muestreo__range'] = (inicio,fin)
				params2['fecha_muestreo__range'] = (inicio,fin)
			if tecnico:
				params['balancenutrientes1__id_tecnico'] = tecnico
				params2['id_tecnico'] = tecnico

			tecnicos_balance = BalanceNutrientes1.objects.filter(**params2).values_list(
								'id_tecnico__nombre_tecnico').distinct('id_tecnico')

			b_lista = []
			b_total_femenino = 0
			b_total_masculino = 0
			b_total_general = 0

			for obj in tecnicos_balance:
				#balance nutrientes
				b_femenino = filtro.filter(**params,
												balancenutrientes1__id_tecnico__nombre_tecnico = obj[0],
												balancenutrientes1__id_productor__sexo = 'femenino').count()
				b_masculino = filtro.filter(**params,
												balancenutrientes1__id_tecnico__nombre_tecnico = obj[0],
												balancenutrientes1__id_productor__sexo = 'masculino').count()
				b_total = b_femenino + b_masculino

				b_lista.append((obj[0],b_femenino,b_masculino,b_total))

				b_total_femenino += b_femenino
				b_total_masculino += b_masculino
				b_total_general += b_total

			#tabla de aplicaciones
			meses = collections.OrderedDict()
			for mes in MESES_CHOICES:
				edaficas = filtro.filter(**params,balancenutrientes1__cafetalesfinca__numeroaplicaciones__mes = mes[0],
										balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion__tipo = 'Edáfica').annotate(
										conteo = Count('balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion')).order_by('-conteo').values_list(
										'balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion__nombre',flat=True).distinct()[:5]
				
				fertilizacion = filtro.filter(**params,balancenutrientes1__cafetalesfinca__numeroaplicaciones__mes = mes[0],
										balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion__tipo = 'Fertilización y sanidad foliar').annotate(
										conteo = Count('balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion')).order_by('-conteo').values_list(
										'balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion__nombre',flat=True).distinct()[:5]
				if edaficas and fertilizacion:
					meses[mes[0]] = edaficas,fertilizacion

	else:
		form = Subfiltros()
		tecnicos_balance = BalanceNutrientes1.objects.values_list(
							'id_tecnico__nombre_tecnico').distinct('id_tecnico')
		b_lista = []
		b_total_femenino = 0
		b_total_masculino = 0
		b_total_general = 0

		for obj in tecnicos_balance:
			#balance nutrientes
			b_femenino = filtro.filter(balancenutrientes1__id_tecnico__nombre_tecnico = obj[0],
											balancenutrientes1__id_productor__sexo = 'femenino').count()
			b_masculino = filtro.filter(balancenutrientes1__id_tecnico__nombre_tecnico = obj[0],
											balancenutrientes1__id_productor__sexo = 'masculino').count()
			b_total = b_femenino + b_masculino

			b_lista.append((obj[0],b_femenino,b_masculino,b_total))

			b_total_femenino += b_femenino
			b_total_masculino += b_masculino
			b_total_general += b_total

		#tabla de aplicaciones
		meses = collections.OrderedDict()
		for mes in MESES_CHOICES:
			edaficas = filtro.filter(balancenutrientes1__cafetalesfinca__numeroaplicaciones__mes = mes[0],
									balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion__tipo = 'Edáfica').annotate(
									conteo = Count('balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion')).order_by('-conteo').values_list(
									'balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion__nombre',flat=True).distinct()[:5]
			
			fertilizacion = filtro.filter(balancenutrientes1__cafetalesfinca__numeroaplicaciones__mes = mes[0],
									balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion__tipo = 'Fertilización y sanidad foliar').annotate(
									conteo = Count('balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion')).order_by('-conteo').values_list(
									'balancenutrientes1__cafetalesfinca__numeroaplicaciones__aplicaciones__tipo_aplicacion__nombre',flat=True).distinct()[:5]
			meses[mes[0]] = edaficas,fertilizacion

		
	return render(request, template, locals())

def monitoreo_campo(request, template = 'monitoreo-campo.html'):
	filtro = _queryset_filtrado(request)

	params_general = {}
	if request.session['comunidad']:
		params_general['comunidad'] = request.session['comunidad']
	if request.session['municipio']:
		params_general['municipio'] = request.session['municipio']
	

	if request.method == 'POST':
		form = Subfiltros(request.POST)
		if form.is_valid():

			inicio = form.cleaned_data['inicio']
			fin = form.cleaned_data['fin']
			tecnico = form.cleaned_data['tecnicos']

			params = {}
			if inicio and fin:
				params['fecha__range'] = (inicio,fin)
			if tecnico:
				params['responsable_at'] = tecnico

			objetivos = {}
			total_d = 0
			total_r = 0
			total_b = 0
			total_mb = 0
			total_e = 0
			for obj in Objetivos.objects.all():
				lista = []
				for x in VALORACION_CHOICES:
					result = MonitoreoCampo1.objects.filter(**params,**params_general,objetivo = obj,valoracion = x[0]).count()
					lista.append(result)
					if x[0] == 'D':
						total_d += result
					elif x[0] == 'R':
						total_r += result
					elif x[0] == 'B':
						total_b += result
					elif x[0] == 'MB':
						total_mb += result
					elif x[0] == 'E':
						total_e += result

				objetivos[obj] = lista	

			#tabla 2
			tecnicos = MonitoreoCampo1.objects.filter(**params,**params_general).values_list('responsable_at',flat = True)
			dict_tecnicos = {}
			for tec in PersonalCampo.objects.filter(id__in = tecnicos):
				dict_tema = {}
				count = 0
				for tema in CatalogoEventos.objects.all():
					lista = []
					for x in VALORACION_CHOICES:
						result = MonitoreoCampo1.objects.filter(**params,**params_general,responsable_at = tec,tema = tema,valoracion = x[0]).count()
						lista.append(result)
					if lista != [0,0,0,0,0]:
						dict_tema[tema] = lista
						count += 1
				dict_tecnicos[tec] = dict_tema,count

	else:
		form = Subfiltros()
		objetivos = {}
		total_d = 0
		total_r = 0
		total_b = 0
		total_mb = 0
		total_e = 0
		for obj in Objetivos.objects.all():
			lista = []
			for x in VALORACION_CHOICES:
				result = MonitoreoCampo1.objects.filter(**params_general,objetivo = obj,valoracion = x[0]).count()
				lista.append(result)
				if x[0] == 'D':
					total_d += result
				elif x[0] == 'R':
					total_r += result
				elif x[0] == 'B':
					total_b += result
				elif x[0] == 'MB':
					total_mb += result
				elif x[0] == 'E':
					total_e += result

			objetivos[obj] = lista	

		#tabla 2
		tecnicos = MonitoreoCampo1.objects.filter(**params_general).values_list('responsable_at',flat = True)
		dict_tecnicos = {}
		for tec in PersonalCampo.objects.filter(id__in = tecnicos):
			dict_tema = {}
			count = 0
			for tema in CatalogoEventos.objects.all():
				lista = []
				for x in VALORACION_CHOICES:
					result = MonitoreoCampo1.objects.filter(**params_general,responsable_at = tec,tema = tema,valoracion = x[0]).count()
					lista.append(result)
				if lista != [0,0,0,0,0]:
					dict_tema[tema] = lista
					count += 1
			dict_tecnicos[tec] = dict_tema,count

	return render(request, template, locals())

def estimado_cosecha(request, template = 'estimado-cosecha.html'):
	filtro = _queryset_filtrado(request)

	if request.method == 'POST':
		form = Subfiltros(request.POST)
		if form.is_valid():

			inicio = form.cleaned_data['inicio']
			fin = form.cleaned_data['fin']
			tecnico = form.cleaned_data['tecnicos']

			params = {}
			if inicio and fin:
				params['estimadocosecha__fecha_recuento__range'] = (inicio,fin)
			if tecnico:
				params['estimadocosecha__id_tecnico'] = tecnico

			total_frutos = filtro.filter(**params,estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion__tipo = 'Daños en frutos y bandolas').aggregate(
								total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']

			total_foliar = filtro.filter(**params,estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion__tipo = 'Daños foliares').aggregate(
									total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
			frutos = {}
			foliares = {}
			for x in SeleccionEstacionEnfermedades.objects.all():
				if x.tipo == 'Daños en frutos y bandolas':
					enfermedad = filtro.filter(**params,estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion = x).aggregate(
									total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
					if enfermedad != 0:
						frutos[x] = saca_porcentajes(enfermedad,total_frutos,False)

				elif x.tipo == 'Daños foliares':
					foliar = filtro.filter(**params,estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion = x).aggregate(
									total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
					if foliar != 0:
						foliares[x] = saca_porcentajes(foliar,total_foliar,False)

	else:
		form = Subfiltros()
		total_frutos = filtro.filter(estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion__tipo = 'Daños en frutos y bandolas').aggregate(
								total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']

		total_foliar = filtro.filter(estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion__tipo = 'Daños foliares').aggregate(
								total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
		frutos = {}
		foliares = {}
		for x in SeleccionEstacionEnfermedades.objects.all():
			if x.tipo == 'Daños en frutos y bandolas':
				enfermedad = filtro.filter(estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion = x).aggregate(
								total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
				if enfermedad != 0:
					frutos[x] = saca_porcentajes(enfermedad,total_frutos,False)

			elif x.tipo == 'Daños foliares':
				foliar = filtro.filter(estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion = x).aggregate(
								total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
				if foliar != 0:
					foliares[x] = saca_porcentajes(foliar,total_foliar,False)

	return render(request, template, locals())

@login_required
def perfiles(request, template = 'list_perfiles.html'):
	filtro = _queryset_filtrado(request)

	if request.method == 'POST':
		form = SubfiltrosPerfiles(request.POST)
		if form.is_valid():

			tipo = form.cleaned_data['tipo']

			# params = {}
			if tipo == '1':
				lista = filtro.filter(mantenimiento__id_productor__in = filtro).distinct('mantenimiento__id_productor')
				print(lista)
			elif tipo == '2':
				lista = filtro.filter(renovacion__id_productor__in = filtro).distinct('renovacion__id_productor')
			elif tipo == '':
				lista = filtro

	else:
		form = SubfiltrosPerfiles()
		lista = filtro

	return render(request, template, locals())

@login_required
def perfiles_detail(request, id, template = 'detail_perfil.html'):
	object = ProductoresGeneral.objects.get(id = id)
	try:
		foto = Documentos.objects.get(tipo_archivo = 'Foto',id_productor = object)
	except:
		pass 

	try:
		acta = Documentos.objects.get(tipo_archivo = 'Acta de Intensión',id_productor = object)
	except:
		pass
	
	try:
		poly = Polygon.objects.filter(productor = object.nombre,id_fa = float(object.id_fa))
		list = []
		for obj in poly:
			for x in obj.mpoly.coords[0]:
				for y in x:
					list.append([y[1],y[0]])
	except:
		pass


	#inventario
	inventarios = InventarioArboles1.objects.filter(id_productor = object)
	especies = ProductoresGeneral.objects.filter(id = object.id).values_list('inventarioarboles1__inventario__especie',flat=True).distinct(
								'inventarioarboles1__inventario__especie').exclude(
								inventarioarboles1__inventario__especie__nombre = None)

	list_especies = []
	arboles = 0
	ab = 0
	vol = 0
	proyecciones = []
	for esp in Especie.objects.filter(id__in = especies):
		crecimiento_anual = 0.35
		prom_dap = ProductoresGeneral.objects.filter(id = object.id).filter(inventarioarboles1__inventario__especie = esp).aggregate(
								total = Coalesce(Avg('inventarioarboles1__inventario__dap'),0))['total']

		total = 0
		lista_proy = []
		for x in range(0,10):
			if x == 0:
				total = prom_dap + crecimiento_anual
			else:
				total = total + crecimiento_anual

			if prom_dap == 0:
				total = 0

			lista_proy.append(total)

		proyecciones.append((esp,prom_dap,crecimiento_anual,lista_proy))

	#rango de anios
	last_year = InventarioArboles1.objects.filter(id_productor = object).values_list('fecha__year',flat=True).distinct('fecha__year').exclude(
								fecha__year = None).order_by('fecha__year').last()
	rango = range(1,11)

	#diagnostico prod
	diagnosticos = DiagnosticoProductivo1.objects.filter(id_productor = object)

	plantas_prod = ProductoresGeneral.objects.filter(id = object.id).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_productiva'),0))['total']
	puntos = ProductoresGeneral.objects.filter(id = object.id).aggregate(total = Coalesce(Count('diagnosticoproductivo1__sublotes'),0))['total'] * 25
	total_cafetos = ProductoresGeneral.objects.filter(id = object.id).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__estimacionmanzana__total_cafetos'),0))['total']

	try:
		porcentaje_plantas_prod = (plantas_prod / puntos) * 100
	except:
		porcentaje_plantas_prod = 0

	plantas_poda = ProductoresGeneral.objects.filter(id = object.id).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_poda_ligera'),0))['total']
	
	try:
		porcentaje_plantas_poda_ligera = (plantas_poda / puntos) * total_cafetos
	except:
		porcentaje_plantas_poda_ligera = 0
	

	plantas_recepo = ProductoresGeneral.objects.filter(id = object.id).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_recepo'),0))['total']
	try:
		porcentaje_plantas_recepo = (plantas_recepo / puntos) * total_cafetos
	except:
		porcentaje_plantas_recepo = 0
	
	planta_requiere_resiembra = ProductoresGeneral.objects.filter(id = object.id).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_requiere_resiembra'),0))['total']
	try:
		porcentaje_planta_requiere_resiembra = (planta_requiere_resiembra / puntos) * total_cafetos
	except:
		porcentaje_planta_requiere_resiembra = 0

	planta_joven = ProductoresGeneral.objects.filter(id = object.id).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__planta_joven'),0))['total']
	try:
		porcentaje_planta_joven = (planta_joven / puntos) * total_cafetos
	except:
		porcentaje_planta_joven = 0
	
	falla_fisica = ProductoresGeneral.objects.filter(id = object.id).aggregate(total = Coalesce(Sum('diagnosticoproductivo1__sublotes__potencialproductivo__falla_fisica'),0))['total']
	try:
		porcentaje_falla_fisica = (falla_fisica / puntos) * total_cafetos
	except:
		porcentaje_falla_fisica = 0

	porcentaje_sombra = ProductoresGeneral.objects.filter(id = object.id).aggregate(total = Coalesce(Avg('diagnosticoproductivo1__sublotes__nivelsombra__nivel_sombra'),0))['total']

	#estimado cosecha
	total_frutos = ProductoresGeneral.objects.filter(id = object.id).filter(estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion__tipo = 'Daños en frutos y bandolas').aggregate(
								total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']

	total_foliar = ProductoresGeneral.objects.filter(id = object.id).filter(estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion__tipo = 'Daños foliares').aggregate(
							total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
	frutos = {}
	foliares = {}
	for x in SeleccionEstacionEnfermedades.objects.all():
		if x.tipo == 'Daños en frutos y bandolas':
			enfermedad = ProductoresGeneral.objects.filter(id = object.id).filter(estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion = x).aggregate(
							total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
			if enfermedad != 0:
				frutos[x] = saca_porcentajes(enfermedad,total_frutos,False)

		elif x.tipo == 'Daños foliares':
			foliar = ProductoresGeneral.objects.filter(id = object.id).filter(estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__seleccion = x).aggregate(
							total = Coalesce(Sum('estimadocosecha__sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
			if foliar != 0:
				foliares[x] = saca_porcentajes(foliar,total_foliar,False)

	estimado_cosecha = EstimadoCosecha.objects.filter(id_productor = object)

	########			
	frutos_totales = EstimadoCosecha.objects.filter(id_productor = object,sublotesestimadocosecha__estacionesenfermedades__seleccion__nombre = 'Frutos totales').aggregate(
							total = Coalesce(Sum('sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
	frutos_bandolas_estrato_medio = EstimadoCosecha.objects.filter(id_productor = object,sublotesestimadocosecha__estacionesenfermedades__seleccion__nombre = 'Frutos bandola estrato medio').aggregate(
							total = Coalesce(Sum('sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']
	
	total_frutos_bandolas = frutos_totales + frutos_bandolas_estrato_medio

	########
	frutos_promedio_bandola = EstimadoCosecha.objects.filter(id_productor = object,sublotesestimadocosecha__estacionesenfermedades__seleccion__nombre = 'Total frutos por bandola').aggregate(
							total = Coalesce(Sum('sublotesestimadocosecha__estacionesenfermedades__total'),0))['total'] / 4

	########
	numero_bandolas_productivas = EstimadoCosecha.objects.filter(id_productor = object,sublotesestimadocosecha__estacionesenfermedades__seleccion__nombre = 'Número de bandolas productivos').aggregate(
							total = Coalesce(Sum('sublotesestimadocosecha__estacionesenfermedades__total'),0))['total']

	total_frutos_planta = frutos_promedio_bandola *  numero_bandolas_productivas

	#######
	distancia_plantas = DiagnosticoProductivo1.objects.filter(id_productor = object).aggregate(
							total = Coalesce(Avg('sublotes__estimacionmanzana__distancia_plantas'),0))['total']
	distancia_surco = DiagnosticoProductivo1.objects.filter(id_productor = object).aggregate(
							total = Coalesce(Avg('sublotes__estimacionmanzana__distancia_surco'),0))['total']
	
	try:
		densidad_siembra = 10000 / (distancia_plantas * distancia_surco)
	except:
		densidad_siembra = 0

	qq_pergamino_oreado = (total_frutos_planta * densidad_siembra) / 82500

	#####
	qq_oro_bruto = qq_pergamino_oreado / 2
	
	#renovacion
	renovaciones = Renovacion.objects.filter(id_productor = object)

	lista_forestal = []
	total_forestales = Renovacion.objects.filter(id_productor = object,plantasrenovacion__especie__tipo = 'Forestal').aggregate(total = Sum('plantasrenovacion__cantidad'))['total']
	forestales = Renovacion.objects.filter(id_productor = object,plantasrenovacion__especie__tipo = 'Forestal').values_list('plantasrenovacion__especie__nombre',flat=True).distinct('plantasrenovacion__especie')

	for x in forestales:
		cantidad =  Renovacion.objects.filter(id_productor = object,plantasrenovacion__especie__nombre = x).aggregate(total = Sum('plantasrenovacion__cantidad'))['total']
		lista_forestal.append((x,cantidad,saca_porcentajes(cantidad,total_forestales,False)))


	lista_frutales = []
	frutales = Renovacion.objects.filter(id_productor = object,plantasrenovacion__especie__tipo = 'Frutal').values_list('plantasrenovacion__especie__nombre',flat=True).distinct('plantasrenovacion__especie')
	total_frutales = Renovacion.objects.filter(id_productor = object,plantasrenovacion__especie__tipo = 'Frutal').aggregate(total = Sum('plantasrenovacion__cantidad'))['total']
	for x in frutales:
		cantidad = Renovacion.objects.filter(id_productor = object,plantasrenovacion__especie__nombre = x).aggregate(total = Sum('plantasrenovacion__cantidad'))['total']
		lista_frutales.append((x,cantidad,saca_porcentajes(cantidad,total_frutales,False)))

	#mantenimiento
	mantenimientos = Mantenimiento.objects.filter(id_productor = object)

	lista_forestal_mant = []
	total_forestales_mant = Mantenimiento.objects.filter(id_productor = object,plantasmantenimiento__especie__tipo = 'Forestal').aggregate(total = Sum('plantasmantenimiento__cantidad'))['total']
	forestales_mant = Mantenimiento.objects.filter(id_productor = object,plantasmantenimiento__especie__tipo = 'Forestal').values_list('plantasmantenimiento__especie__nombre',flat=True).distinct('plantasmantenimiento__especie')

	for x in forestales_mant:
		cantidad =  Mantenimiento.objects.filter(id_productor = object,plantasmantenimiento__especie__nombre = x).aggregate(total = Sum('plantasmantenimiento__cantidad'))['total']
		lista_forestal_mant.append((x,cantidad,saca_porcentajes(cantidad,total_forestales_mant,False)))


	lista_frutales_mant = []
	frutales_mant = Mantenimiento.objects.filter(id_productor = object,plantasmantenimiento__especie__tipo = 'Frutal').values_list('plantasmantenimiento__especie__nombre',flat=True).distinct('plantasmantenimiento__especie')
	total_frutales_mant = Mantenimiento.objects.filter(id_productor = object,plantasmantenimiento__especie__tipo = 'Frutal').aggregate(total = Sum('plantasmantenimiento__cantidad'))['total']
	for x in frutales_mant:
		cantidad = Mantenimiento.objects.filter(id_productor = object,plantasmantenimiento__especie__nombre = x).aggregate(total = Sum('plantasmantenimiento__cantidad'))['total']
		lista_frutales_mant.append((x,cantidad,saca_porcentajes(cantidad,total_frutales_mant,False)))

	####### balance de nutrientes
	balances = BalanceNutrientes1.objects.filter(id_productor = object)
	prod_cafe = balances.aggregate(total = Coalesce(Sum('cafetalesfinca__produccioncafetal__prod_cafe_pergamino'),0))['total']
	banano = balances.aggregate(total = Coalesce(Sum('cafetalesfinca__produccioncafetal__banano'),0))['total']
	lenia = balances.aggregate(total = Coalesce(Sum('cafetalesfinca__produccioncafetal__lenia'),0))['total']
	
	especies_balance = balances.values_list('cafetalesfinca__produccionfrutales__especie__nombre').distinct('cafetalesfinca__produccionfrutales__especie')
	list_balance = []
	for x in especies_balance:
		conteo = balances.filter(cafetalesfinca__produccionfrutales__especie__nombre = x[0]).aggregate(total = Coalesce(Sum('cafetalesfinca__produccionfrutales__cantidad'),0))['total']
		list_balance.append((x[0],conteo))

	###### capacitaciones
	capacitaciones = Capacitaciones.objects.filter(productores = object)

	###### monitoreo campo
	monitoreo = MonitoreoCampo1.objects.filter(productor = object)

	###### asistencia tecnica
	asist = AsistenciaTecnica.objects.filter(id_productor = object)

	return render(request, template, locals())

def update_prod_fundacion(request):
	update_productores.delay()
	return HttpResponseRedirect('/admin') 

def update_prod_aldea(request):
	update_productores_aldea.delay()
	return HttpResponseRedirect('/admin')

def get_munis(request):
	ids = request.GET.get('ids', '')
	dicc = {}
	resultado = []
	if ids:
		lista = ids.split(',')
		for id in lista:
			try:
				depto = Departamento.objects.get(id = id)
				prod = ProductoresGeneral.objects.filter(comunidad__municipio__departamento = depto.id).values_list('comunidad__municipio__id',flat=True).distinct()
				municipios = Municipio.objects.filter(departamento__id = depto.id,id__in = prod).order_by('nombre')
				lista1 = []
				for municipio in municipios:
					muni = {}
					muni['id'] = municipio.id
					muni['nombre'] = municipio.nombre
					lista1.append(muni)
					dicc[depto.nombre] = lista1
			except:
				pass

	resultado.append(dicc)

	return HttpResponse(simplejson.dumps(resultado), content_type = 'application/json')

def get_comunies(request):
	ids = request.GET.get('ids', '')
	dicc = {}
	resultado = []
	if ids:
		lista = ids.split(',')
		for id in lista:
			try:
				munici = Municipio.objects.get(id = id)
				prod = ProductoresGeneral.objects.filter(comunidad__municipio = munici.id).values_list('comunidad__id',flat=True).distinct()
				comunidades = Comunidad.objects.filter(municipio__id = munici.id,id__in = prod).order_by('nombre')
				lista1 = []
				for comunidad in comunidades:
					comu = {}
					comu['id'] = comunidad.id
					comu['nombre'] = comunidad.nombre
					lista1.append(comu)
					dicc[munici.nombre] = lista1
			except:
				pass

	resultado.append(dicc)

	return HttpResponse(simplejson.dumps(resultado), content_type = 'application/json')

def saca_porcentajes(dato, total, formato=True):
	if dato != None:
		try:
			porcentaje = (dato/float(total)) * 100 if total != None or total != 0 else 0
		except:
			return 0
		if formato:
			return porcentaje
		else:
			return '%.2f' % porcentaje
	else:
		return 0

from dal import autocomplete

class MunicipioAutocomplete(autocomplete.Select2QuerySetView):
	def get_queryset(self):

		qs = Municipio.objects.all()

		if self.q:
			qs = qs.filter(nombre__istartswith=self.q)

		return qs

class ComunidadAutocomplete(autocomplete.Select2QuerySetView):
	def get_queryset(self):

		qs = Comunidad.objects.all()

		municipio = self.forwarded.get('municipio', None)

		if municipio:
			qs = qs.filter(municipio=municipio)

		if self.q:
			qs = qs.filter(nombre__istartswith=self.q)

		return qs

class ProductorAutocomplete(autocomplete.Select2QuerySetView):
	def get_queryset(self):
		qs = ProductoresGeneral.objects.all()

		comunidad = self.forwarded.get('comunidad', None)

		if comunidad:
			qs = qs.filter(comunidad=comunidad)

		if self.q:
			qs = qs.filter(Q(id_fa__istartswith = self.q) | Q(nombre__istartswith=self.q))

		return qs

class SeleccionAutocomplete(autocomplete.Select2QuerySetView):
	def get_queryset(self):

		qs = SeleccionEstacionEnfermedades.objects.all()

		if self.q:
			qs = qs.filter(nombre__istartswith=self.q)

		return qs

def admin_tables(request, template = 'admin/tablas.html'):
	renovacion = Renovacion.objects.all()
	plantasrenovacion = PlantasRenovacion.objects.all()
	##
	mantenimiento = Mantenimiento.objects.all()
	plantasmantenimiento = PlantasMantenimiento.objects.all()
	##
	diagnostico = DiagnosticoProductivo1.objects.all()
	puntos = Sublotes.objects.all()
	estimadomanzana = EstimacionManzana.objects.all()
	potencialproductivo = PotencialProductivo.objects.all()
	nivelsombra = NivelSombra.objects.all()
	coberturasuelo = CoberturaSuelo.objects.all()
	##
	estimado = EstimadoCosecha.objects.all()
	sublotesestimado = SublotesEstimadoCosecha.objects.all()
	estaciones1 = EstacionesEnfermedades.objects.all()
	estaciones2 = EstacionesEnfermedades2.objects.all()
	##
	balancenutrientes = BalanceNutrientes1.objects.all()
	cafetales = CafetalesFinca.objects.all()
	numeroaplicaciones = NumeroAplicaciones.objects.all()
	aplicaciones = Aplicaciones.objects.all()
	produccioncafetal = ProduccionCafetal.objects.all()
	produccionfrutales = ProduccionFrutales.objects.all()

	##
	inventarioforestal = InventarioArboles1.objects.all()
	inventario = Inventario.objects.all()

	return render(request, template, locals())