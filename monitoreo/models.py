# -*- coding: UTF-8 -*-
from django.db import models
from django.contrib.gis.db import models as polygon
from lugar.models import *
from catalogo.models import *
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.

SEXO_CHOICES = (('femenino','femenino'),('masculino','masculino'))

ESTADO_CHOICES = (('retirado','retirado'),('activo','activo'),('no asociado/a','no asociado/a'),('suspendido','suspendido'))

TIPO_CHOICES = (('SAF-Mantenimiento','SAF-Mantenimiento'),('SAF-Renovación','SAF-Renovación'),
				('Sin Clasificación','Sin Clasificación'))

class Productores(models.Model):
	nombre = models.CharField(max_length=250)
	cedula = models.CharField(max_length=50,null=True,blank=True)
	sexo = models.CharField(max_length=20,choices=SEXO_CHOICES)
	telefono = models.CharField(max_length=50,null=True,blank=True)
	nivel_educativo = models.CharField(max_length=100,null=True,blank=True)
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING)
	comunidad_finca = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,verbose_name='Comunidad de la finca',related_name='comunidada_finca_prod',null=True,blank=True)
	direccion_finca = models.CharField(max_length=200,null=True,blank=True)
	distancia_cabezera_departamental = models.FloatField(null=True,blank=True)
	estado = models.CharField(max_length=20,choices=ESTADO_CHOICES)
	tipo = models.CharField(max_length=100,choices=TIPO_CHOICES)
	# id_tecnico = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Responsable asistencia técnica')
	# x = models.FloatField(null=True,blank=True)
	# y = models.FloatField(null=True,blank=True)
	latitud = models.FloatField(null=True,blank=True)
	longitud = models.FloatField(null=True,blank=True)
	# error_gps = models.FloatField(null=True,blank=True)
	# elevacion = models.FloatField(null=True,blank=True)
	update = models.BooleanField(editable=False)

	class Meta:
		verbose_name = 'Productor Fundación'
		verbose_name_plural = '2. Productores Fundación'

	def __str__(self):
		return self.nombre

	def save(self, *args, **kwargs):
		self.update = False
		super(Productores, self).save(*args, **kwargs)

class ProductoresAldea(models.Model):
	nombre = models.CharField(max_length=250)
	cedula = models.CharField(max_length=50,null=True,blank=True)
	sexo = models.CharField(max_length=20,choices=SEXO_CHOICES)
	telefono = models.CharField(max_length=50,null=True,blank=True)
	nivel_educativo = models.CharField(max_length=100,null=True,blank=True)
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,null=True,blank=True)
	comunidad_finca = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,verbose_name='Comunidad de la finca',related_name='comunidada_finca',null=True,blank=True)
	direccion_finca = models.CharField(max_length=200,null=True,blank=True)
	distancia_cabezera_departamental = models.FloatField(null=True,blank=True)
	estado = models.CharField(max_length=20,choices=ESTADO_CHOICES)
	tipo = models.CharField(max_length=100,choices=TIPO_CHOICES)
	# id_tecnico = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Responsable asistencia técnica')
	# x = models.FloatField(null=True,blank=True)
	# y = models.FloatField(null=True,blank=True)
	latitud = models.FloatField(null=True,blank=True)
	longitud = models.FloatField(null=True,blank=True)
	# error_gps = models.FloatField(null=True,blank=True)
	# elevacion = models.FloatField(null=True,blank=True)
	update = models.BooleanField(editable=False)

	class Meta:
		verbose_name = 'Productor Aldea'
		verbose_name_plural = '1. Productores Aldea'

	def __str__(self):
		return self.nombre

	def save(self, *args, **kwargs):
		self.update = False
		super(ProductoresAldea, self).save(*args, **kwargs)

class ProductoresGeneral(models.Model):
	id_fa = models.IntegerField(null=True,blank=True)
	id_aldea = models.IntegerField(null=True,blank=True)
	nombre = models.CharField(max_length=250)
	cedula = models.CharField(max_length=50,null=True,blank=True)
	sexo = models.CharField(max_length=20,choices=SEXO_CHOICES)
	telefono = models.CharField(max_length=50,null=True,blank=True)
	nivel_educativo = models.CharField(max_length=100,null=True,blank=True)
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,null=True,blank=True)
	comunidad_finca = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,verbose_name='Comunidad de la finca',related_name='comunidada_finca_general',null=True,blank=True)
	direccion_finca = models.CharField(max_length=200,null=True,blank=True)
	distancia_cabezera_departamental = models.FloatField(null=True,blank=True)
	estado = models.CharField(max_length=20,choices=ESTADO_CHOICES)
	# tipo = models.CharField(max_length=100,choices=TIPO_CHOICES)
	# id_tecnico = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Responsable asistencia técnica')
	latitud = models.FloatField(null=True,blank=True)
	longitud = models.FloatField(null=True,blank=True)

	class Meta:
		verbose_name = 'Productor General'
		verbose_name_plural = 'Productores General'

	def __str__(self):
		return '%s - %s / %s' % (self.id_fa,self.nombre,self.cedula)

DOC_CHOICES = (('Acta de Intensión','Acta de Intensión'),('Foto','Foto'))

class Documentos(models.Model):
	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.CASCADE)
	tipo_archivo = models.CharField(max_length=50,choices=DOC_CHOICES)
	archivo = models.FileField(upload_to='documentos/')

	class Meta:
		verbose_name = 'Documento'
		verbose_name_plural = 'Documentos'

	def save(self, *args, **kwargs):
		try:
			this = Documentos.objects.get(id=self.id)
			if this.archivo != self.archivo:
				this.archivo.delete()
		except: 
			pass
		super(Documentos, self).save(*args, **kwargs)

class SubirPoligono(models.Model):
	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.CASCADE)
	archivo = models.FileField(upload_to='data/')

	class Meta:
		verbose_name = 'Archivo Poligono'
		verbose_name_plural = 'Archivos Poligonos'

	#sobreescribir archivo con mismo nombre
	def save(self, *args, **kwargs):
		try:
			this = SubirPoligono.objects.get(id=self.id)
			if this.archivo != self.archivo:
				this.archivo.delete()
		except: 
			pass
		super(SubirPoligono, self).save(*args, **kwargs)

class Polygon(models.Model):
	id = models.CharField(primary_key=True,max_length=200,editable=False)
	# id_productor = models.ForeignKey(Productores,on_delete=models.DO_NOTHING,blank=True,null=True)
	area_ha = models.FloatField()
	area_mz = models.FloatField()
	productor = models.CharField(max_length=50)
	comunidad = models.CharField(max_length=50)
	municipio = models.CharField(max_length=50)
	mpoly = polygon.MultiPolygonField()
	id_fa = models.FloatField()

	def save(self, *args, **kwargs):
		self.id = str(self.area_ha) +'-'+ str(self.productor) +'-'+ str(self.id_fa)
		super(Polygon, self).save(*args, **kwargs)

	def __str__(self):
		return self.productor

	class Meta:
		verbose_name = 'Polígono'
		verbose_name_plural = '7. Polígonos'

class Renovacion(models.Model):
	_index = models.AutoField(primary_key=True)
	municipio = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar municipio para filtrar productor')
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar comunidad para filtrar productor')
	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,verbose_name='Productor/a')
	fecha_ingreso = models.DateField()
	fecha_entrega = models.DateField()
	responsable_at = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Responsable AT',related_name='responsable_at')
	responsable_entrega = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Responsable de entrega')
	numero_requisa = models.IntegerField(verbose_name='N° Requisa',blank=True,null=True)
	area_mz = models.FloatField()
	# area_ha = models.FloatField()
	usuario = models.ForeignKey(User,on_delete=models.DO_NOTHING)

	class Meta:
		verbose_name = 'Entrega de plantas SAF-Renovación'
		verbose_name_plural = '1. Entrega de plantas SAF-Renovación'

	def __str__(self):
		return self.id_productor.nombre

class PlantasRenovacion(models.Model):
	parent_index = models.ForeignKey(Renovacion,on_delete=models.CASCADE)
	especie = models.ForeignKey(Especie,on_delete=models.DO_NOTHING)
	cantidad = models.IntegerField()

	class Meta:
		verbose_name_plural = 'Plantas'

class Mantenimiento(models.Model):
	_index = models.AutoField(primary_key=True)
	municipio = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar municipio para filtrar productor')
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar comunidad para filtrar productor')
	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,verbose_name='Productor/a')
	fecha_ingreso = models.DateField()
	fecha_entrega = models.DateField()
	responsable_at = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Responsable AT',related_name='responsable_at_mant')
	responsable_entrega = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Responsable de entrega')
	numero_requisa = models.IntegerField(null=True,blank=True,verbose_name='N° Requisa')
	area_mz = models.FloatField()
	# area_ha = models.FloatField()
	numero_requisa = models.IntegerField(verbose_name='N° Requisa',blank=True,null=True)
	usuario = models.ForeignKey(User,on_delete=models.DO_NOTHING)
	
	class Meta:
		verbose_name = 'Entrega de plantas SAF-Mantenimiento'
		verbose_name_plural = '2. Entrega de plantas SAF-Mantenimiento'

	def __str__(self):
		return self.id_productor.nombre

class PlantasMantenimiento(models.Model):
	parent_index = models.ForeignKey(Mantenimiento,on_delete=models.CASCADE)
	especie = models.ForeignKey(Especie,on_delete=models.DO_NOTHING)
	cantidad = models.IntegerField()

	class Meta:
		verbose_name_plural = 'Plantas'

SI_NO_CHOICES = (('si','si'),('no','no'))

# MONITOREO_CHOICES = (('Capacitaciones y talleres','Capacitaciones y talleres'),
# 					('Establecimiento de SAF','Establecimiento de SAF'),
# 					('Sobrevivencia','Sobrevivencia'))

VALORACION_CHOICES = (('D','Deficiente'),('R','Regular'),('B','Bueno'),('MB','Muy Bueno'),('E','Excelente'),)

class MonitoreoCampo1(models.Model):
	responsable_seguimiento = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Responsable de seguimiento')
	responsable_at = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Responsable AT',related_name='responsable_at_monitoreo')
	fecha = models.DateField()
	municipio = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING)
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING)
	tema = models.ForeignKey(CatalogoEventos,on_delete=models.DO_NOTHING,null=True,blank=True)
	productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,null=True,blank=True)
	objetivo = models.ForeignKey(Objetivos,on_delete=models.DO_NOTHING)
	comentarios = models.TextField(null=True,blank=True)
	valoracion = models.CharField(max_length=2,choices=VALORACION_CHOICES)
	usuario = models.ForeignKey(User,on_delete=models.DO_NOTHING)

	class Meta:
		verbose_name = 'Monitoreo de campo'
		verbose_name_plural = '1. Monitoreo de campo'

	# def __str__(self):
	# 	return self.id_productor

# CALIFICACION_CHOICES = (('A','A'),('B','B'),('C','C'),('D','D'),)

# class CapacitacionesTalleres(models.Model):
# 	parent_index = models.ForeignKey(MonitoreoCampo1,on_delete=models.CASCADE)
# 	responsable_at = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,related_name='responsable_at_cap')
# 	responsable_evento = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING)
# 	dominio_facilitador = models.CharField(max_length=1,choices=CALIFICACION_CHOICES,verbose_name='Nivel de dominio del facilitador')
# 	conceptualizacion = models.CharField(max_length=1,choices=CALIFICACION_CHOICES,verbose_name='Conceptualización, usos y beneficios')
# 	disenio_metodologico = models.CharField(max_length=1,choices=CALIFICACION_CHOICES,verbose_name='Desarrollo de diseño metodológico')
# 	materiales_didacticos = models.CharField(max_length=1,choices=CALIFICACION_CHOICES,verbose_name='Uso de materiales didácticos adecuados')
# 	coordinacion_previa = models.CharField(max_length=1,choices=CALIFICACION_CHOICES,verbose_name='Coordinación previa')
# 	distribucion_tiempo = models.CharField(max_length=1,choices=CALIFICACION_CHOICES,verbose_name='Distribución del tiempo')
# 	evaluacion = models.CharField(max_length=1,choices=CALIFICACION_CHOICES,verbose_name='Evaluación expost /asociados')
# 	cantidad_participantes = models.IntegerField(verbose_name='Cantidad de participantes')

# 	class Meta:
# 		verbose_name_plural = 'Capacitaciones y talleres'

# class EstablecimientoSaf(models.Model):
# 	parent_index = models.ForeignKey(MonitoreoCampo1,on_delete=models.CASCADE)
# 	# comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING)
# 	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,verbose_name='Productor/a')
# 	responsable_at = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING)
# 	distancia_siembra_forestales_plantas = models.FloatField()
# 	distancia_siembra_forestales_surcos = models.FloatField()
# 	distancia_siembra_musaceas_plantas = models.FloatField()
# 	distancia_siembra_musaceas_surcos = models.FloatField()
# 	variaciones_diseno_plantas = models.CharField(max_length=2,choices=SI_NO_CHOICES)
# 	variaciones_diseno_surcos = models.CharField(max_length=2,choices=SI_NO_CHOICES) 
# 	causas_variaciones = models.TextField(null=True,blank=True)

# 	class Meta:
# 		verbose_name_plural = 'Implementación modelos SAF'

# class Sobrevivencia(models.Model):
# 	parent_index = models.ForeignKey(MonitoreoCampo1,on_delete=models.CASCADE)
# 	# comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING)
# 	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,verbose_name='Productor/a')

# 	class Meta:
# 		verbose_name = 'Adaptación especies forestales'
# 		verbose_name_plural = 'Adaptación especies forestales'

# class EspeciesSobrevivencia(models.Model):
# 	parent_index = models.ForeignKey(Sobrevivencia,on_delete=models.CASCADE)
# 	especie = models.ForeignKey(Especie,on_delete=models.DO_NOTHING)
# 	establecidas = models.IntegerField()
# 	no_establecidas = models.IntegerField()
# 	factores_causantes = models.ManyToManyField(FactoresCausantes,blank=True)

TIPO_EVENTO_CHOICES = (('Sesion de Trabajo','Sesion de Trabajo'),('Dia de campo','Dia de campo'),('Taller','Taller'),
						('Charla','Charla'),('Gira de Intercambio','Gira de Intercambio'),
						('Seminario','Seminario'),('Curso','Curso'),('Post Grado','Post Grado'),
						('Parcela Demostrativa','Parcela Demostrativa'),('Asesoria','Asesoria'),
						('Pasantías','Pasantías'),('Asamblea Comunitaria','Asamblea Comunitaria'))

class Capacitaciones(models.Model):
	fecha_evento = models.DateField()
	municipio_capacitacion = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING,related_name='municipio_cap')
	comunidad_capacitacion = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,related_name='comunidad_cap')
	codigo_proyecto = models.ForeignKey(Proyectos,on_delete=models.DO_NOTHING)
	codigo_evento = models.ForeignKey(CatalogoEventos,on_delete=models.DO_NOTHING)
	tipo_evento = models.CharField(max_length=50,choices=TIPO_EVENTO_CHOICES)
	responsable_evento = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING)
	lugar_evento = models.CharField(max_length=200)
	municipio = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar municipio para filtrar productor')
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar comunidad para filtrar productor')
	productores = models.ManyToManyField(ProductoresGeneral)
	usuario = models.ForeignKey(User,on_delete=models.DO_NOTHING)

	class Meta:
		verbose_name = 'Capacitaciones'
		verbose_name_plural = '2. Capacitaciones'

LOTE_DIAGNOSTICO = ((1,'1'),(2,'2'),(3,'3'),(4,'4'),(5,'5'))

class DiagnosticoProductivo1(models.Model):
	id_tecnico = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Técnico')
	municipio = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar municipio para filtrar productor')
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar comunidad para filtrar productor')
	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,verbose_name='Productor/a')
	fecha_recuento = models.DateField()
	cantidad_sublotes = models.IntegerField('Número del Lote',choices=LOTE_DIAGNOSTICO)
	usuario = models.ForeignKey(User,on_delete=models.DO_NOTHING)

	class Meta:
		verbose_name = 'Diagnóstico Productivo'
		verbose_name_plural = '3. Diagnóstico Productivo'

	def __str__(self):
		return self.id_productor.nombre

LOTE_CHOICES = ((1,'1'),(2,'2'),(3,'3'))
PUNTOS_CHOICES = ((1,'1'),(2,'2'),(3,'3'),(4,'4'))

class Sublotes(models.Model):
	parent_index = models.ForeignKey(DiagnosticoProductivo1,on_delete=models.CASCADE)
	sublotes = models.IntegerField(choices=PUNTOS_CHOICES,verbose_name='Puntos')

	class Meta:
		verbose_name = 'Punto'
		verbose_name_plural = 'Puntos'

class EstimacionManzana(models.Model):
	parent_index = models.ForeignKey(Sublotes,on_delete=models.CASCADE)
	distancia_surco = models.FloatField(verbose_name='Distancia entre surco (varas)')
	distancia_plantas = models.FloatField(verbose_name='Distancia entre plantas (varas)')
	total_cafetos = models.IntegerField(verbose_name='Total de cafetos por mz',editable=False)

	class Meta:
		verbose_name_plural = 'Densidad de siembra'

	def save(self, *args, **kwargs):
		self.total_cafetos = self.distancia_surco * self.distancia_plantas
		super(EstimacionManzana, self).save(*args, **kwargs)

class PotencialProductivo(models.Model):
	parent_index = models.ForeignKey(Sublotes,on_delete=models.CASCADE)
	variedad = models.ForeignKey(VariedadCafe,on_delete=models.DO_NOTHING)
	planta_productiva = models.IntegerField()
	planta_poda_ligera = models.IntegerField()
	planta_recepo = models.IntegerField()
	planta_requiere_resiembra = models.IntegerField()
	planta_joven = models.IntegerField()
	falla_fisica = models.IntegerField()

	class Meta:
		verbose_name_plural = 'Potencial productivo de los cafetos'

class NivelSombra(models.Model):
	parent_index = models.ForeignKey(Sublotes,on_delete=models.CASCADE)
	nivel_sombra = models.IntegerField()

	class Meta:
		verbose_name_plural = 'Nivel de sombra en cada estación'

TIPO_COBERTURA_CHOICES = (
		('Zacate/ Coyolillo','Zacate/ Coyolillo'),
		('Hoja ancha anual','Hoja ancha anual'),
		('Hoja ancha perenne','Hoja ancha perenne'),
		('Bejuco en calle','Bejuco en calle'),
		('Malezas cortadas','Malezas cortadas'),
		('Suelo desnudo','Suelo desnudo'),
		('Hojarasca ','Hojarasca ')
	)

class CoberturaSuelo(models.Model):
	parent_index = models.ForeignKey(Sublotes,on_delete=models.CASCADE)
	tipo_cobertura = models.CharField(max_length=100,choices=TIPO_COBERTURA_CHOICES)
	porcentaje = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(100)])

	class Meta:
		verbose_name_plural = 'Cobertura de suelo principal de manera visual'


class EstimadoCosecha(models.Model):
	municipio = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar municipio para filtrar productor')
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar comunidad para filtrar productor')
	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,verbose_name='Productor/a')
	fecha_recuento = models.DateField()
	id_tecnico = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Técnico')
	usuario = models.ForeignKey(User,on_delete=models.DO_NOTHING)

	class Meta:
		verbose_name = 'Estimado de cosecha'
		verbose_name_plural = '4. Estimado de cosecha'

	def __str__(self):
		return '%s' % (self.id_productor.nombre)

class SublotesEstimadoCosecha(models.Model):
	parent_index = models.ForeignKey(EstimadoCosecha,on_delete=models.CASCADE)
	sublotes = models.IntegerField(choices=PUNTOS_CHOICES,verbose_name='Punto')

	class Meta:
		verbose_name_plural = 'Puntos'

ESTIMADO_CHOICES = (
		('1.Hojas con roya','1.Hojas con roya'),('2.Hojas con mancha de hierro','2.Hojas con mancha de hierro'),
		('3.Hojas con ojo de gallo','3.Hojas con ojo de gallo'),('4.Hojas con antracnosis','4.Hojas con antracnosis'),
		('5.Hojas con otra enfermedad','5.Hojas con otra enfermedad'),('6.Hojas con minador de la hoja','6.Hojas con minador de la hoja'),
		('7.Hojas totales por bandola','7.Hojas totales por bandola'),('8.% rama muerte antracnosis','8.% rama muerte antracnosis'),
		('9.Frutos con broca','9.Frutos con broca'),('10.Frutos con chasparia','10.Frutos con chasparia'),
		('11.Frutos con antracnosis','11.Frutos con antracnosis'),('12.Frutos totales','12.Frutos totales'),
		('13.Nudos con cochinilla','13.Nudos con cochinilla'),('14.Nudos productivos','14.Nudos productivos'),
		('15.Total nudos','15.Total nudos'),('16.Frutos bandolas estrato medio','16.Frutos bandolas estrato medio'),
		('17.Total frutos bandolas (12+16)','17.Total frutos bandolas (12+16)'),('18.Frutos promedio X bandolas (17/4)','18.Frutos promedio X bandolas (17/4)'),
		('19.Numero bandolas productivas','19.Numero bandolas productivas'),('20.Total frutos por planta(18x19)','20.Total frutos por planta(18x19)')
	)

class EstacionesEnfermedades(models.Model):
	parent_index = models.ForeignKey(SublotesEstimadoCosecha,on_delete=models.CASCADE)
	seleccion = models.ForeignKey(SeleccionEstacionEnfermedades,on_delete=models.DO_NOTHING)
	total = models.IntegerField(validators=[MinValueValidator(0)])

	class Meta:
		verbose_name_plural = '1.1 Plagas, enfermedades en hojas, frutos y recuento'

class EstacionesEnfermedades2(models.Model):
	parent_index = models.ForeignKey(SublotesEstimadoCosecha,on_delete=models.CASCADE)
	plantas_cafe = models.IntegerField(verbose_name='21.Plantas de café con pellejillo ')
	sombra_estacion = models.IntegerField(verbose_name='22.% Sombra en la estación')

	class Meta:
		verbose_name_plural = '1.2 Plagas, enfermedades en hojas, frutos y recuento'

class BalanceNutrientes1(models.Model):
	id_tecnico = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Técnico')
	municipio = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar municipio para filtrar productor')
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar comunidad para filtrar productor')
	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,verbose_name='Productor/a')
	fecha_muestreo = models.DateField()
	usuario = models.ForeignKey(User,on_delete=models.DO_NOTHING)

	class Meta:
		verbose_name = 'Balance de Nutrientes'
		verbose_name_plural = '5. Balance de Nutrientes'

	def __str__(self):
		return '%s' % (self.id_productor.nombre)

class CafetalesFinca(models.Model):
	parent_index = models.ForeignKey(BalanceNutrientes1,on_delete=models.CASCADE)
	lote = models.IntegerField(choices=LOTE_CHOICES)
	area_mz = models.FloatField()
	edad_cafetal = models.FloatField()
	variedad = models.ForeignKey(VariedadCafe,on_delete=models.DO_NOTHING)
	sombra = models.FloatField(verbose_name='% sombra',validators=[MinValueValidator(0),MaxValueValidator(100)])
	cantidad_servicios = models.IntegerField()
	cantidad_maderables = models.IntegerField()
	cantidad_frutales = models.IntegerField()
	cantidad_banano = models.IntegerField()

	class Meta:
		verbose_name = 'Lotes en finca'

MESES_CHOICES = (('Enero','Enero'),('Febrero','Febrero'),('Marzo','Marzo'),('Abril','Abril'),
                ('Mayo','Mayo'),('Junio','Junio'),('Julio','Julio'),('Agosto','Agosto'),
                ('Septiembre','Septiembre'),('Octubre','Octubre'),('Noviembre','Noviembre'),
                ('Diciembre','Diciembre'))

APLICACIONES_CHOICES = ((1,'1'),(2,'2'),(3,'3'),(4,'4'))

class NumeroAplicaciones(models.Model):
	parent_index = models.ForeignKey(CafetalesFinca,on_delete=models.CASCADE)
	numero_aplicacion = models.IntegerField(choices=APLICACIONES_CHOICES)
	mes = models.CharField(max_length=30,choices=MESES_CHOICES)

class Aplicaciones(models.Model):
	parent_index = models.ForeignKey(NumeroAplicaciones,on_delete=models.CASCADE)
	tipo_aplicacion = models.ForeignKey(FormulaQuimica,on_delete=models.DO_NOTHING)
	cantidad = models.FloatField()
	
	class Meta:
		verbose_name_plural = 'Aplicaciones'

class ProduccionCafetal(models.Model):
	parent_index = models.ForeignKey(CafetalesFinca,on_delete=models.CASCADE)	
	prod_cafe_pergamino = models.FloatField(verbose_name='Producción café qq (pergamino oreado)')
	banano = models.FloatField()
	lenia = models.FloatField(verbose_name='Leña de café o guaba (marca/lote)')

	class Meta:
		verbose_name = 'Producción de cada cafetal'

class ProduccionFrutales(models.Model):
	parent_index = models.ForeignKey(CafetalesFinca,on_delete=models.CASCADE)
	especie = models.ForeignKey(Especie,on_delete=models.DO_NOTHING)
	cantidad = models.IntegerField()

	class Meta:
		verbose_name = 'Producción frutales'
		verbose_name_plural = 'Producción frutales'

MEDIDAS_CHOICES = (('Zanjas de drenaje','Zanjas de drenaje'),('Barreras vivas','Barreras vivas'),
					('Cercas vivas','Cercas vivas'),('Cortinas rompe viento','Cortinas rompe viento'),
					('Control de malesas','Control de malesas'),('Poda sanitaria','Poda sanitaria'),
					('Poda de formación','Poda de formación'))

TIPO_SUELO_CHOICES = (('Arcilloso','Arcilloso'),('Franco arcilloso','Franco arcilloso'),('Arenoso','Arenoso'),
						('Otros','Otros'))

FUENTES_AGUA_CHOICES = (('Quebrada','Quebrada'),('Pozo','Pozo'),('Ojo de agua','Ojo de agua'),
						('Agua potable','Agua potable'),('Otros','Otros'))

USOS_AGUA_CHOICES = (('Uso domiciliar','Uso domiciliar'),('Beneficiado de cafe','Beneficiado de cafe'),('Riego','Riego'),
						('Ganaderia','Ganaderia'),('Otros','Otros'))


class InventarioArboles1(models.Model):
	municipio = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar municipio para filtrar productor')
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar comunidad para filtrar productor')
	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,verbose_name='Productor/a')
	fecha = models.DateField()
	encuestador  = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Técnico')
	usuario = models.ForeignKey(User,on_delete=models.DO_NOTHING)

	class Meta:
		verbose_name = 'Inventario forestal'
		verbose_name_plural = '6. Inventario forestal'

	def __str__(self):
		return '%s' % (self.id_productor.nombre)

class Inventario(models.Model):
	parent_index = models.ForeignKey(InventarioArboles1,on_delete=models.CASCADE)
	especie = models.ForeignKey(Especie,on_delete=models.DO_NOTHING)
	sobrevivencia = models.IntegerField()
	dap = models.FloatField(null=True,blank=True)
	area_basal = models.FloatField(null=True,blank=True)
	volumen = models.FloatField(null=True,blank=True)
	anio_establecimiento = models.IntegerField(verbose_name='Año establecimiento')

	class Meta:
		verbose_name_plural = 'Inventario forestal'

TIPO_VISITA = (('Monitoreo','Monitoreo'),('Asistencia técnica','Asistencia técnica'))
TIPO_VISITA2 = (('Mantenimiento','Mantenimiento'),('Renovación','Renovación'))

class AsistenciaTecnica(models.Model):
	tipo_visita = models.CharField(max_length=30,choices=TIPO_VISITA)
	tipo = models.CharField(max_length=30,choices=TIPO_VISITA2)
	municipio = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar municipio para filtrar productor')
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING,null=True,blank=True,verbose_name='Seleccionar comunidad para filtrar productor')
	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,verbose_name='Productor/a')
	id_tecnico  = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Técnico')
	fecha_visita = models.DateField()
	objetivo = models.ManyToManyField(Objetivos)
	situacion_encontrada = models.TextField()
	acuerdos = models.TextField()
	recomendaciones = models.TextField()
	observaciones = models.TextField()
	hora_llegada = models.TimeField()
	hora_salida = models.TimeField()
	usuario = models.ForeignKey(User,on_delete=models.DO_NOTHING)

	class Meta:
		verbose_name = 'Visita'
		verbose_name_plural = '3. Visitas'

	def __str__(self):
		return '%s' % (self.id_productor)

class EntregaHerramientas(models.Model):
	municipio = models.ForeignKey(Municipio,on_delete=models.DO_NOTHING)
	comunidad = models.ForeignKey(Comunidad,on_delete=models.DO_NOTHING)
	id_productor = models.ForeignKey(ProductoresGeneral,on_delete=models.DO_NOTHING,verbose_name='Productor/a')
	fecha_entrega = models.DateField()
	responsable_at = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Responsable AT',related_name='responsable_at_herramientas')
	responsable_entrega = models.ForeignKey(PersonalCampo,on_delete=models.DO_NOTHING,verbose_name='Responsable de entrega')

	class Meta:
		verbose_name = 'Entrega de herramienta'
		verbose_name_plural = '4. Entrega de herramientas'	

	def __str__(self):
		return '%s' % (self.id_productor)

class DetalleEntrega(models.Model):
	parent_index = models.ForeignKey(EntregaHerramientas,on_delete=models.CASCADE)
	herramienta = models.ForeignKey(Herramienta,on_delete=models.DO_NOTHING)
	cantidad = models.FloatField()

	class Meta:
		verbose_name_plural = 'Detalle entrega'