# -*- coding: UTF-8 -*-
from django.db import models
from .models import *
from django import forms
from django.forms.models import BaseInlineFormSet
from django.core.exceptions import ValidationError
from dal import autocomplete

class RenovacionForm(forms.ModelForm):
    class Meta:
        model = Renovacion
        fields = "__all__"
        widgets = {
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete'),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'id_productor': autocomplete.ModelSelect2(url='prod-autocomplete',forward=['comunidad'])
        }

class MantenimientoForm(forms.ModelForm):
    class Meta:
        model = Mantenimiento
        fields = "__all__"
        widgets = {
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete'),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'id_productor': autocomplete.ModelSelect2(url='prod-autocomplete',forward=['comunidad'])
        }

class MonitoreoCampoForm(forms.ModelForm):
    class Meta:
        model = MonitoreoCampo1
        fields = "__all__"
        widgets = {
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete'),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'id_productor': autocomplete.ModelSelect2(url='prod-autocomplete',forward=['comunidad'])
        }

class CapacitacionesForm(forms.ModelForm):
    class Meta:
        model = Capacitaciones
        fields = "__all__"
        widgets = {
            'municipio_capacitacion': autocomplete.ModelSelect2(url='muni-autocomplete'),
            'comunidad_capacitacion': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete'),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'productores': autocomplete.ModelSelect2Multiple(url='prod-autocomplete',forward=['comunidad'])
        }

class EstimadoCosechaForm(forms.ModelForm):
    class Meta:
        model = EstimadoCosecha
        fields = "__all__"
        widgets = {
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete'),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'id_productor': autocomplete.ModelSelect2(url='prod-autocomplete',forward=['comunidad'])
        }

class DiagnosticoProductivoForm(forms.ModelForm):
    class Meta:
        model = DiagnosticoProductivo1
        fields = "__all__"
        widgets = {
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete'),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'id_productor': autocomplete.ModelSelect2(url='prod-autocomplete',forward=['comunidad'])
        }

class BalanceNutrientesForm(forms.ModelForm):
    class Meta:
        model = BalanceNutrientes1
        fields = "__all__"
        widgets = {
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete'),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'id_productor': autocomplete.ModelSelect2(url='prod-autocomplete',forward=['comunidad'])
        }

class InventarioArbolesForm(forms.ModelForm):
    class Meta:
        model = InventarioArboles1
        fields = "__all__"
        widgets = {
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete'),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'id_productor': autocomplete.ModelSelect2(url='prod-autocomplete',forward=['comunidad'])
        }

class AsistenciaTecnicaForm(forms.ModelForm):
    class Meta:
        model = AsistenciaTecnica
        fields = "__all__"
        widgets = {
            'municipio': autocomplete.ModelSelect2(url='muni-autocomplete'),
            'comunidad': autocomplete.ModelSelect2(url='comu-autocomplete',forward=['municipio']),
            'id_productor': autocomplete.ModelSelect2(url='prod-autocomplete',forward=['comunidad'])
        }

def departamentos():   
    foo = ProductoresGeneral.objects.all().order_by('comunidad__municipio__departamento__nombre').distinct(
                                                    'comunidad__municipio__departamento__nombre').values_list(
                                                    'comunidad__municipio__departamento__id', flat=True)
    return Departamento.objects.filter(id__in = foo)

SEXO_CHOICES = (('femenino','femenino'),('masculino','masculino'),('','ambos'))

ESTADO_CHOICES = (('','-----'),('retirado','retirado'),('activo','activo'),('no asociado/a','no asociado/a'),('suspendido','suspendido'))

TIPO_CHOICES = (('','-----'),('SAF-Mantenimiento','SAF-Mantenimiento'),('SAF-Renovación','SAF-Renovación'),
                ('Sin Clasificación','Sin Clasificación'))

class FiltrosProductor(forms.Form):
    def __init__(self, *args, **kwargs):
        super(FiltrosProductor, self).__init__(*args, **kwargs)
        self.fields['departamento'] = forms.ModelMultipleChoiceField(queryset=departamentos(), required=True, label=u'Departamentos')
        self.fields['municipio'] = forms.ModelMultipleChoiceField(queryset=Municipio.objects.all().order_by('nombre'), required=False)
        self.fields['comunidad'] = forms.ModelMultipleChoiceField(queryset=Comunidad.objects.all(), required=False)
        self.fields['sexo'] = forms.ChoiceField(label=u'Sexo',choices=SEXO_CHOICES,required=False,widget=forms.RadioSelect)
        self.fields['estado'] = forms.ChoiceField(label=u'Estado',choices=ESTADO_CHOICES,required=False)
        # self.fields['tipo'] = forms.ChoiceField(label=u'Tipo',choices=TIPO_CHOICES,required=False)

class Subfiltros(forms.Form):
    def __init__(self, *args, **kwargs):
        super(Subfiltros, self).__init__(*args, **kwargs)
        self.fields['inicio'] = forms.DateField(required=False,widget=forms.DateTimeInput(attrs={
            'class': 'form-control',
            'type': 'date'
        }))
        self.fields['fin'] = forms.DateField(required=False,widget=forms.DateTimeInput(attrs={
            'class': 'form-control',
            'type': 'date'
        }))
        
        self.fields['tecnicos'] = forms.ModelChoiceField(queryset=PersonalCampo.objects.all(), required=False,widget=forms.Select(attrs={
            'class': 'form-control'
        }))

class Subfiltros2(forms.Form):
    def __init__(self, *args, **kwargs):
        super(Subfiltros2, self).__init__(*args, **kwargs)
        self.fields['inicio'] = forms.DateField(required=False,widget=forms.DateTimeInput(attrs={
            'class': 'form-control',
            'type': 'date'
        }))
        self.fields['fin'] = forms.DateField(required=False,widget=forms.DateTimeInput(attrs={
            'class': 'form-control',
            'type': 'date'
        }))

TIPO_CHOICES = (('','Todos'),(1,'SAF-Mantenimiento'),(2,'SAF-Renovación'))

class SubfiltrosPerfiles(forms.Form):
    def __init__(self, *args, **kwargs):
        super(SubfiltrosPerfiles, self).__init__(*args, **kwargs)
        self.fields['tipo'] = forms.ChoiceField(label=u'Tipo',choices=TIPO_CHOICES,required=False)

#validacion
# class CoberturaSueloFormSet(BaseInlineFormSet):

# 	def clean(self):
# 		super(CoberturaSueloFormSet, self).clean()

# 		percent = 0
# 		for form in self.forms:
# 			if not hasattr(form, 'cleaned_data'):
# 				continue
# 			data = form.cleaned_data
# 			percent += data.get('porcentaje', 0)

# 		if percent != 100:
# 			raise ValidationError(_('Total de los elementos debe de ser 100%%. Actual : %(percent).2f%%') % {'percent': percent})

	