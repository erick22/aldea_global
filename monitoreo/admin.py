from django.contrib import admin
from .models import *
from import_export.admin import ImportExportModelAdmin,ExportMixin
from import_export import resources
from import_export.widgets import *
from import_export.fields import Field
from aldea_global.tasks import *
from .forms import *
import nested_admin

# Register your models here.
class SubirPoligono_Inline(admin.TabularInline):
    model = SubirPoligono
    extra = 1

class Documentos_Inline(admin.TabularInline):
    model = Documentos
    extra = 1

class ProductoresResource(resources.ModelResource):
    class Meta:
        model = Productores
        fields = ('id','nombre','cedula','sexo','telefono','nivel_educativo','comunidad','comunidad_finca','direccion_finca','distancia_cabezera_departamental',
                    'estado','tipo','latitud','longitud')
    
class ProductoresAdmin(ImportExportModelAdmin):
    autocomplete_fields = ['comunidad','comunidad_finca']
    # inlines = [Documentos_Inline,SubirPoligono_Inline,]
    list_display = ('id','nombre','cedula','sexo','comunidad','tipo','estado')
    search_fields = ['id','nombre','cedula','comunidad__nombre']
    list_filter = ('sexo','tipo','estado')
    list_display_links = ('id', 'nombre')
    resource_class = ProductoresResource

   
class ProductoresAldeaResource(resources.ModelResource):
    class Meta:
        model = ProductoresAldea
        fields = ('id','nombre','cedula','sexo','telefono','nivel_educativo','comunidad','comunidad_finca','direccion_finca','distancia_cabezera_departamental',
                    'estado','latitud','longitud')

class ProductoresAldeaAdmin(ImportExportModelAdmin):
    autocomplete_fields = ['comunidad','comunidad_finca']
    list_display = ('id','nombre','cedula','sexo','comunidad','estado')
    search_fields = ['id','nombre','cedula','comunidad__nombre']
    list_filter = ('sexo','tipo','estado')
    list_display_links = ('id', 'nombre')
    resource_class = ProductoresAldeaResource

class EstacionesEnfermedades2Inline(nested_admin.NestedTabularInline):
    model = EstacionesEnfermedades2
    max_num = 1
    can_delete = False

class EstacionesEnfermedadesInline(nested_admin.NestedTabularInline):
    model = EstacionesEnfermedades
    extra = 1
    # autocomplete_fields = ('seleccion')

class SublotesEstimadoCosechaInline(nested_admin.NestedTabularInline):
    model = SublotesEstimadoCosecha
    extra = 1 
    max_num = 4  
    inlines = [EstacionesEnfermedadesInline,EstacionesEnfermedades2Inline]

class EstimadoCosechaAdmin(nested_admin.NestedModelAdmin):
    inlines = [SublotesEstimadoCosechaInline]
    form = EstimadoCosechaForm
    autocomplete_fields = ('id_tecnico','usuario')
    list_display = ('id_productor','id_tecnico','fecha_recuento')
    search_fields = ('id_productor__nombre','id_productor__id_fa')
    date_hierarchy = ('fecha_recuento')

    # class Media:
    #   js = ('/static/js/fix_inlines.js',)

    def get_queryset(self, request):
        if request.user.is_superuser:
            return EstimadoCosecha.objects.all()
        return EstimadoCosecha.objects.filter(usuario=request.user)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            obj.save()
        else:
            obj.usuario = request.user
            obj.save()

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.exclude = ('usuario',)
        else:
            self.exclude = ()
        return super(EstimadoCosechaAdmin, self).get_form(request, obj=None, **kwargs) 

class PlantasRenovacionInline(admin.TabularInline):
    model = PlantasRenovacion
    extra = 1
    # autocomplete_fields = ('especie',)

class RenovacionAdmin(admin.ModelAdmin):
    form = RenovacionForm
    inlines = [PlantasRenovacionInline,]
    autocomplete_fields = ('responsable_at','responsable_entrega','usuario')
    list_display = ('id_productor','responsable_at','fecha_entrega')
    search_fields = ('id_productor__nombre','id_productor__id_fa')
    date_hierarchy = ('fecha_entrega')

    def get_queryset(self, request):
        if request.user.is_superuser:
            return Renovacion.objects.all()
        return Renovacion.objects.filter(usuario=request.user)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            obj.save()
        else:
            obj.usuario = request.user
            obj.save()

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.exclude = ('usuario',)
        else:
            self.exclude = ()
        return super(RenovacionAdmin, self).get_form(request, obj=None, **kwargs) 

class PlantasMantenimientoInline(admin.TabularInline):
    model = PlantasMantenimiento
    extra = 1
    # autocomplete_fields = ('especie',)

class MantenimientoAdmin(admin.ModelAdmin):
    form = MantenimientoForm
    inlines = [PlantasMantenimientoInline,]
    autocomplete_fields = ('responsable_at','responsable_entrega','usuario')
    list_display = ('id_productor','responsable_at','fecha_entrega')
    search_fields = ('id_productor__nombre','id_productor__id_fa')
    date_hierarchy = ('fecha_entrega')

    def get_queryset(self, request):
        if request.user.is_superuser:
            return Mantenimiento.objects.all()
        return Mantenimiento.objects.filter(usuario=request.user)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            obj.save()
        else:
            obj.usuario = request.user
            obj.save()

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.exclude = ('usuario',)
        else:
            self.exclude = ()
        return super(MantenimientoAdmin, self).get_form(request, obj=None, **kwargs) 

# class CapacitacionesTalleresInline(nested_admin.NestedTabularInline):
#     model = CapacitacionesTalleres
#     extra = 1
#     autocomplete_fields = ('responsable_at','responsable_evento')

# class EstablecimientoSafInline(nested_admin.NestedTabularInline):
#     model = EstablecimientoSaf
#     extra = 1
#     autocomplete_fields = ('id_productor','responsable_at')

# class EspeciesSobrevivenciaInline(nested_admin.NestedTabularInline):
#     model = EspeciesSobrevivencia
#     extra = 1
#     # autocomplete_fields = ('especie',)

# class SobrevivenciaInline(nested_admin.NestedTabularInline):
#     model = Sobrevivencia
#     extra = 1
#     inlines = [EspeciesSobrevivenciaInline,]
#     autocomplete_fields = ('id_productor',)

class MonitoreoCampo1Admin(nested_admin.NestedModelAdmin,ImportExportModelAdmin):
    form = MonitoreoCampoForm
    # inlines = [CapacitacionesTalleresInline,EstablecimientoSafInline,SobrevivenciaInline]
    list_display = ('municipio','comunidad','fecha')
    date_hierarchy = ('fecha')
    autocomplete_fields = ('usuario','objetivo','responsable_seguimiento','responsable_at','productor','tema')

    def get_queryset(self, request):
        if request.user.is_superuser:
            return MonitoreoCampo1.objects.all()
        return MonitoreoCampo1.objects.filter(usuario=request.user)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            obj.save()
        else:
            obj.usuario = request.user
            obj.save()

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.exclude = ('usuario',)
        else:
            self.exclude = ()
        return super(MonitoreoCampo1Admin, self).get_form(request, obj=None, **kwargs) 

class EstimacionManzanaInline(nested_admin.NestedTabularInline):
    model = EstimacionManzana
    max_num = 1
    can_delete = False

class PotencialProductivoInline(nested_admin.NestedTabularInline):
    model = PotencialProductivo
    max_num = 1
    extra = 1
    can_delete = False
    # autocomplete_fields = ('variedad')

class NivelSombraInline(nested_admin.NestedTabularInline):
    model = NivelSombra
    max_num = 1
    can_delete = False

class CoberturaSueloInline(nested_admin.NestedTabularInline):
    model = CoberturaSuelo
    max_num = 4
    extra = 4

class SublotesInline(nested_admin.NestedTabularInline):
    model = Sublotes
    max_num = 4
    extra = 1
    inlines = [EstimacionManzanaInline,PotencialProductivoInline,NivelSombraInline,CoberturaSueloInline]

class DiagnosticoProductivo1Admin(nested_admin.NestedModelAdmin):
    inlines = [SublotesInline]
    form = DiagnosticoProductivoForm
    autocomplete_fields = ('id_tecnico','usuario')
    list_display = ('id_productor','id_tecnico','fecha_recuento')
    search_fields = ('id_productor__nombre','id_productor__id_fa')

    def get_queryset(self, request):
        if request.user.is_superuser:
            return DiagnosticoProductivo1.objects.all()
        return DiagnosticoProductivo1.objects.filter(usuario=request.user)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            obj.save()
        else:
            obj.usuario = request.user
            obj.save()

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.exclude = ('usuario',)
        else:
            self.exclude = ()
        return super(DiagnosticoProductivo1Admin, self).get_form(request, obj=None, **kwargs) 

class AplicacionesInline(nested_admin.NestedTabularInline):
    model = Aplicaciones
    extra = 1
    # autocomplete_fields = ('tipo_aplicacion',)

class NumeroAplicacionesInline(nested_admin.NestedTabularInline):
    model = NumeroAplicaciones
    extra = 1
    max_num = 4
    inlines = [AplicacionesInline,]

class ProduccionCafetalInline(nested_admin.NestedTabularInline):
    model = ProduccionCafetal
    max_num = 1

class ProduccionFrutalesInline(nested_admin.NestedTabularInline):
    model = ProduccionFrutales
    extra = 1
    # autocomplete_fields = ('especie',)

    def formfield_for_foreignkey(self,db_field,request=None,**kwargs):
        if db_field.name == 'especie':
            kwargs["queryset"] = Especie.objects.filter(tipo = 'Frutal')
        return super(ProduccionFrutalesInline,self).formfield_for_foreignkey(db_field,request,**kwargs)

class CafetalesFincaInline(nested_admin.NestedStackedInline):
    model = CafetalesFinca
    max_num = 3
    extra = 1
    inlines = [NumeroAplicacionesInline,ProduccionCafetalInline,ProduccionFrutalesInline,]
    # autocomplete_fields = ('variedad',)

    fieldsets = [
        (None,{'fields':['lote']}),
        ('Cafetales en finca',{'fields':['area_mz','edad_cafetal','variedad','sombra']}),
        ('Tipo de árbol predominante',{'fields':['cantidad_servicios','cantidad_maderables','cantidad_frutales','cantidad_banano']}),
    ]

class BalanceNutrientes1Admin(nested_admin.NestedModelAdmin):
    inlines = [CafetalesFincaInline,]
    # InsumosCafetalInline,ProduccionCafetalInline,ProduccionFrutalesInline
    form = BalanceNutrientesForm
    autocomplete_fields = ('id_tecnico','usuario')
    list_display = ('id_productor','id_tecnico','fecha_muestreo')
    date_hierarchy = ('fecha_muestreo')
    search_fields = ('id_productor__nombre','id_productor__id_fa')

    def get_queryset(self, request):
        if request.user.is_superuser:
            return BalanceNutrientes1.objects.all()
        return BalanceNutrientes1.objects.filter(usuario=request.user)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            obj.save()
        else:
            obj.usuario = request.user
            obj.save()

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.exclude = ('usuario',)
        else:
            self.exclude = ()
        return super(BalanceNutrientes1Admin, self).get_form(request, obj=None, **kwargs) 

class InventarioInline(admin.TabularInline):
    model = Inventario
    extra = 1
    # autocomplete_fields = ('especie',)

class InventarioArboles1Admin(admin.ModelAdmin):
    form = InventarioArbolesForm
    inlines = [InventarioInline,]
    autocomplete_fields = ('encuestador','usuario',)
    list_display = ('id_productor','encuestador','fecha')
    date_hierarchy = ('fecha')
    search_fields = ('id_productor__nombre','id_productor__id_fa')

    def get_queryset(self, request):
        if request.user.is_superuser:
            return InventarioArboles1.objects.all()
        return InventarioArboles1.objects.filter(usuario=request.user)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            obj.save()
        else:
            obj.usuario = request.user
            obj.save()

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.exclude = ('usuario',)
        else:
            self.exclude = ()
        return super(InventarioArboles1Admin, self).get_form(request, obj=None, **kwargs) 

class PolygonAdmin(ImportExportModelAdmin):
    list_display = ('id_fa','productor','comunidad','municipio','area_ha')
    list_display_links = ('id_fa', 'productor')
    search_fields = ['productor','comunidad','municipio']

class CapacitacionesAdmin(ImportExportModelAdmin):
    form = CapacitacionesForm
    list_display = ('id','fecha_evento','lugar_evento','responsable_evento','codigo_evento','tipo_evento')
    date_hierarchy = 'fecha_evento'
    search_fields = ['id','lugar_evento__nombre','responsable_evento__nombre_tecnico']
    list_filter = ('tipo_evento',)
    list_display_links = ('id','fecha_evento','lugar_evento')
    autocomplete_fields = ('responsable_evento','usuario','codigo_proyecto','codigo_evento')
    # filter_horizontal = ('productores',)


    def get_queryset(self, request):
        if request.user.is_superuser:
            return Capacitaciones.objects.all()
        return Capacitaciones.objects.filter(usuario=request.user)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            obj.save()
        else:
            obj.usuario = request.user
            obj.save()

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.fieldsets = [
                (None, {'fields':[('fecha_evento'),('lugar_evento'),('municipio_capacitacion'),('comunidad_capacitacion'),('codigo_proyecto'),('codigo_evento'),('tipo_evento'),('responsable_evento')]}),
                ('Filtrar productores', {'fields':[('municipio','comunidad'),('productores')]}),
            ]
        else:
            self.fieldsets = [
                (None, {'fields':[('fecha_evento'),('lugar_evento'),('municipio_capacitacion'),('comunidad_capacitacion'),('codigo_proyecto'),('codigo_evento'),('tipo_evento'),('responsable_evento'),('usuario')]}),
                ('Filtrar productores', {'fields':[('municipio','comunidad'),('productores')]}),
            ]
        return super(CapacitacionesAdmin, self).get_form(request, obj=None, **kwargs) 

class AsistenciaTecnicaAdmin(ImportExportModelAdmin):
    form = AsistenciaTecnicaForm
    autocomplete_fields = ('usuario','id_tecnico')
    list_display = ('id_productor','fecha_visita')
    date_hierarchy = ('fecha_visita')
    search_fields = ('id_productor__nombre','id_productor__id_fa')

    def get_queryset(self, request):
        if request.user.is_superuser:
            return AsistenciaTecnica.objects.all()
        return AsistenciaTecnica.objects.filter(usuario=request.user)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            obj.save()
        else:
            obj.usuario = request.user
            obj.save()

    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            self.exclude = ('usuario',)
        else:
            self.exclude = ()
        return super(AsistenciaTecnicaAdmin, self).get_form(request, obj=None, **kwargs) 


class ProductoresGeneralAdmin(ImportExportModelAdmin):
    list_display = ('id','id_fa','id_aldea','nombre','estado')
    autocomplete_fields = ['comunidad','comunidad_finca']
    list_display_links = ('id','id_fa','id_aldea','nombre')
    search_fields = ('nombre','id_fa')
    inlines = [Documentos_Inline,SubirPoligono_Inline,]

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        ids = []
        for instance in instances:
            instance.save()
            ids.append(instance.id)
        formset.save()

        save_polygon.delay(ids)


class DetalleEntregaInline(admin.TabularInline):
    model = DetalleEntrega
    extra = 1

class EntregaHerramientasAdmin(admin.ModelAdmin):
    inlines = [DetalleEntregaInline]

# admin.site.register(Productores,ProductoresAdmin)
# admin.site.register(ProductoresAldea,ProductoresAldeaAdmin)
admin.site.register(EstimadoCosecha,EstimadoCosechaAdmin)
admin.site.register(Renovacion,RenovacionAdmin)
admin.site.register(Mantenimiento,MantenimientoAdmin)
admin.site.register(MonitoreoCampo1,MonitoreoCampo1Admin)
admin.site.register(DiagnosticoProductivo1,DiagnosticoProductivo1Admin)
admin.site.register(BalanceNutrientes1,BalanceNutrientes1Admin)
admin.site.register(InventarioArboles1,InventarioArboles1Admin)
admin.site.register(Capacitaciones,CapacitacionesAdmin)
admin.site.register(AsistenciaTecnica,AsistenciaTecnicaAdmin)

admin.site.register(Polygon,PolygonAdmin)

admin.site.register(ProductoresGeneral,ProductoresGeneralAdmin)
admin.site.register(EntregaHerramientas,EntregaHerramientasAdmin)

from django.contrib.auth.admin import UserAdmin

class UserResource(resources.ModelResource):
    class Meta:
        model = User
        fields = ('id', 'username')

class UserAdmin(ExportMixin, UserAdmin):
    resource_class = UserResource

admin.site.unregister(User)
admin.site.register(User, UserAdmin)