import os, fnmatch
from django.contrib.gis.utils import LayerMapping
from .models import Polygon, Productores
from django.contrib.gis.gdal import DataSource

mapping = {
	# 'id' : 'Id',
	'area_ha' : 'AREA_HA',
	'area_mz' : 'AREA_MZ',
	'productor' : 'PRODUCTOR',
	'comunidad' : 'COMUNIDAD',
	'municipio' : 'MUNICIPIO',
	'mpoly' : 'MULTIPOLYGON',
}

def run(verbose=True):
	listOfFiles = os.listdir('monitoreo/data/')  
	pattern = "*.shp"
	for entry in listOfFiles:  
		if fnmatch.fnmatch(entry, pattern):
			shp = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', ''+ entry),)
			lm = LayerMapping(Polygon, shp, mapping, transform=True)
			lm.save(strict=True, verbose=verbose)
			
			#get nombre del productor
			ds = DataSource(shp)
			lyr = ds[0]
			srs = lyr.srs
			feat = lyr[0]
			prod = (feat.get('PRODUCTOR'))

			#asignar id_prodcutor al poligono
			# q = Productores.objects.get(nombre = prod)
			# poly = Polygon.objects.get(productor = q.nombre)
			# poly.id_productor = q
			# poly.save()

			