from django.contrib import admin
from .models import *
from import_export.admin import ImportExportModelAdmin
from import_export import resources
from import_export.widgets import *
from import_export.fields import Field

# Register your models here.
class PersonalCampoAdmin(ImportExportModelAdmin):
	search_fields = ['nombre_tecnico',]

class ProyectosAdmin(ImportExportModelAdmin):
	search_fields = ['nombre_proyecto','codigo','donante']

class ConsultoresAdmin(ImportExportModelAdmin):
	search_fields = ['nombre_consultor',]

class CatalogoEventosAdmin(ImportExportModelAdmin):
	search_fields = ['tema','grupo','linea_presupuestaria']

class EspeciesAdmin(ImportExportModelAdmin):
    list_display = ('nombre','nombre_cientifico','tipo')
    list_filter = ('tipo',)
    search_fields = ['nombre','nombre_cientifico',]

class FormulaQuimicaAdmin(ImportExportModelAdmin):
    list_display = ('nombre','tipo')
    list_filter = ('tipo',)
    search_fields = ['nombre','tipo']

class VariedadCafeAdmin(ImportExportModelAdmin):
    search_fields = ['nombre',]

class ObjetivosAdmin(ImportExportModelAdmin):
    search_fields = ['nombre',]

class SeleccionEstacionEnfermedadesAdmin(ImportExportModelAdmin):
    search_fields = ['nombre',]

admin.site.register(Proyectos,ProyectosAdmin)
admin.site.register(Consultores,ConsultoresAdmin)
admin.site.register(PersonalCampo,PersonalCampoAdmin)
admin.site.register(CatalogoEventos,CatalogoEventosAdmin)
admin.site.register(Especie,EspeciesAdmin)
admin.site.register(VariedadCafe,VariedadCafeAdmin)
admin.site.register(FormulaQuimica,FormulaQuimicaAdmin)
admin.site.register(Herramienta)
admin.site.register(FactoresCausantes)
admin.site.register(SeleccionEstacionEnfermedades,SeleccionEstacionEnfermedadesAdmin)
admin.site.register(Objetivos,ObjetivosAdmin)
