# -*- coding: UTF-8 -*-
from django.db import models

# Create your models here.
class PersonalCampo(models.Model):
	nombre_tecnico = models.CharField(max_length=250)

	class Meta:
		verbose_name_plural = '08. Personal de campo'
		ordering = ['nombre_tecnico',]

	def __str__(self):
		return self.nombre_tecnico

class Consultores(models.Model):
	nombre_consultor = models.CharField(max_length=250)

	class Meta:
		verbose_name_plural = '01. Consultores'
		ordering = ['nombre_consultor',]

	def __str__(self):
		return self.nombre_consultor

class Proyectos(models.Model):
	nombre_proyecto = models.CharField(max_length=250)
	codigo = models.CharField(max_length=50)
	donante = models.CharField(max_length=250)

	class Meta:
		verbose_name_plural = '10. Proyectos'
		ordering = ['nombre_proyecto',]

	def __str__(self):
		return self.nombre_proyecto

class CatalogoEventos(models.Model):
	tema = models.CharField(max_length=200)
	grupo = models.CharField(max_length=200)
	linea_presupuestaria = models.CharField(max_length=200)

	class Meta:
		verbose_name_plural = '03. Eventos'
		ordering = ['tema',]

	def __str__(self):
		return '%s - %s' % (self.linea_presupuestaria,self.tema)

TIPO_ESPECIES = (('Frutal','Frutal'),('Forestal','Forestal'),('Musácea','Musácea'))

class Especie(models.Model):
	nombre = models.CharField(max_length=100)
	nombre_cientifico = models.CharField(max_length=100)
	tipo = models.CharField(max_length=20,choices=TIPO_ESPECIES)

	class Meta:
		verbose_name = 'Especie'
		verbose_name_plural = '02. Especies'
		ordering = ['nombre','tipo']

	def __str__(self):
		return '%s - %s' % (self.nombre,self.tipo)

class FactoresCausantes(models.Model):
	nombre = models.CharField(max_length=100)

	class Meta:
		verbose_name = 'Factor Causante'
		verbose_name_plural = '04. Factores Causantes'

	def __str__(self):
		return self.nombre

TIPO_FORMULA_CHOICES = (('Edáfica','Edáfica'),('Enmienda','Enmienda'),('Fertilización y sanidad foliar','Fertilización y sanidad foliar'))

UNIDAD_CHOICES = (('Unidad','Unidad'),('Quintal','Quintal'),('Libra','Libra'),('Litro','Litro'),
					('Gramo','Gramo'),('Kilogramo','Kilogramo'),('Onza','Onza'))

class FormulaQuimica(models.Model):
	nombre = models.CharField(max_length=100)
	tipo = models.CharField(max_length=30,choices=TIPO_FORMULA_CHOICES)
	unidad_medida = models.CharField(max_length=30,choices=UNIDAD_CHOICES)
	casa_comercial = models.CharField(max_length=100,blank=True,null=True)
	composicion_quimica = models.TextField(blank=True,null=True)

	class Meta:
		verbose_name = 'Fórmula química'
		verbose_name_plural = '05. Fórmulas químicas'
		ordering = ['nombre','tipo']

	def __str__(self):
		return '%s - %s - %s' % (self.nombre,self.tipo,self.unidad_medida)

class Herramienta(models.Model):
	nombre = models.CharField(max_length=100)
	unidad = models.CharField(max_length=20,choices=UNIDAD_CHOICES)

	class Meta:
		verbose_name = 'Herramienta'
		verbose_name_plural = '06. Herramientas'

	def __str__(self):
		return '%s - %s' % (self.nombre,self.unidad)

class Objetivos(models.Model):
	nombre = models.CharField(max_length=200)

	def __str__(self):
		return '%s' % (self.nombre)

	class Meta:
		verbose_name = 'Objetivo'
		verbose_name_plural = '07. Objetivos'
		ordering = ['nombre',]

CLASIFICACION_ENFERMEDADES = (('Daños foliares','Daños foliares'),
								('Daños en frutos y bandolas','Daños en frutos y bandolas'),
								('Estimado de Cosecha','Estimado de Cosecha'))

class SeleccionEstacionEnfermedades(models.Model):
	nombre = models.CharField(max_length=100)
	tipo  = models.CharField(max_length=50,choices=CLASIFICACION_ENFERMEDADES)

	def __str__(self):
		return '%s' % (self.nombre)

	class Meta:
		verbose_name_plural = '09. Plagas, enfermedades en hojas, frutos y recuento'

class VariedadCafe(models.Model):
	nombre = models.CharField(max_length=100)

	class Meta:
		verbose_name = 'Variedad Café'
		verbose_name_plural = '11. Variedades Café'

	def __str__(self):
		return self.nombre

class Temas(models.Model):
	nombre = models.CharField(max_length=100)

	class Meta:
		verbose_name = 'Tema'
		verbose_name_plural = '12. Temas'

	def __str__(self):
		return self.nombre