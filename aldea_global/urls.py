"""aldea_global URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from monitoreo.views import *
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

admin.site.site_header = 'Sistema Digital SAF-Café'
admin.site.site_title = 'SAF-Café'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('admin/tables/', admin_tables),
    path('nested_admin/', include('nested_admin.urls')),
    path('', index),
    path('consulta/', consulta),
    path('ajax/municipios/', get_munis, name='get-munis'),
    path('ajax/comunidades/', get_comunies, name='get-comunies'),
    path('update-prod-fundacion', update_prod_fundacion),
    path('update-prod-aldea', update_prod_aldea),
    path('inventario-forestal/', inventario_forestal),
    path('asistencia-tecnica/', asistencia_tecnica),
    path('capacitaciones/', capacitaciones),
    path('diagnostico-productivo/', diagnostico_productivo),
    path('balance-nutrientes/', balance_nutrientes),
    path('renovacion/', renovacion),
    path('mantenimiento/', mantenimiento),
    path('monitoreo-campo/', monitoreo_campo),
    path('estimado-cosecha/', estimado_cosecha),
    path('perfiles/', perfiles),
    path('perfiles/<int:id>', perfiles_detail, name='detail-perfil'),

    path('login/', auth_views.LoginView.as_view(template_name='login.html')),
    path('logout/', auth_views.LogoutView.as_view(next_page = '/')),

    path('municipio-autocomplete/',
        MunicipioAutocomplete.as_view(create_field='nombre'),
        name='muni-autocomplete',
    ),

    path(
        'comunidad-autocomplete/',
        ComunidadAutocomplete.as_view(create_field='nombre'),
        name='comu-autocomplete',
    ),

    path(
        'productor-autocomplete/',
        ProductorAutocomplete.as_view(create_field='nombre'),
        name='prod-autocomplete',
    ),

    path(
        'seleccion-autocomplete/',
        SeleccionAutocomplete.as_view(create_field='nombre'),
        name='seleccion-autocomplete',
    ),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
