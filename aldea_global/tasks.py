# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task,Task
import os, fnmatch
from monitoreo.models import *
from django.contrib.gis.utils import LayerMapping
from django.contrib.gis.gdal import DataSource
from django.db.models import Q
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.contrib.auth.models import User

mapping = {
	# 'id' : 'Id',
	'area_ha' : 'AREA_HA',
	'area_mz' : 'AREA_MZ',
	'productor' : 'PRODUCTOR',
	'comunidad' : 'COMUNIDAD',
	'municipio' : 'MUNICIPIO',
	'mpoly' : 'MULTIPOLYGON',
	'id_fa' : 'Id_FA',
}

@shared_task(default_retry_delay=10, max_retries=3)
def save_polygon(ids):
	pattern = "*.shp"
	print(ids)
	poly = SubirPoligono.objects.filter(id__in = ids)
	for x in poly:  
		if fnmatch.fnmatch(x.archivo.name, pattern):
			shp = os.path.abspath(os.path.join('media', ''+ x.archivo.name),)
			lm = LayerMapping(Polygon, shp, mapping, transform=True)
			lm.save(strict=True, verbose=True)

			# #get id del polygono
			# ds = DataSource(shp)
			# lyr = ds[0]
			# srs = lyr.srs
			# feat = lyr[0]
			# id = (feat.get('Id'))

			# # asignar id_prodcutor al poligono
			# polygon = Polygon.objects.get(id = id)
			# print(x.id_productor,'prod',polygon)
			# polygon.id_productor = x.id_productor
			# polygon.save()

class CallbackTask(Task):
	def on_success(self, retval, task_id, args, kwargs):
		subject, from_email = 'Actualización BD Productores Fundación', 'no.reply.aldea@mail.com'
		text_content =  render_to_string('email/actualizacion_bd.txt', {'retval': retval,})

		html_content = render_to_string('email/actualizacion_bd.txt', {'retval': retval,})

		superusers_emails = User.objects.filter(is_superuser=True).values_list('email',flat=True)

		msg = EmailMultiAlternatives(subject, text_content, from_email, superusers_emails)
		msg.attach_alternative(html_content, "text/html")
		msg.send()

@shared_task(default_retry_delay=10, max_retries=3,base=CallbackTask)
def update_productores():
	prod_nuevos = Productores.objects.filter(update = False).exclude(id__in = ProductoresGeneral.objects.exclude(id_fa__isnull=True).values_list('id_fa',flat=True))

	update_prod = ProductoresGeneral.objects.filter(Q(id_fa__in = Productores.objects.filter(update = False).values_list('id',flat=True)) |
													Q(cedula__in = Productores.objects.filter(update = False).values_list('cedula',flat=True)))

	if update_prod:
		for obj in update_prod:
			p = Productores.objects.get(Q(id = obj.id_fa) | Q(cedula = obj.cedula))
			obj.id_fa = p.id
			obj.nombre = p.nombre
			obj.cedula = p.cedula
			obj.sexo = p.sexo
			obj.telefono = p.telefono
			obj.nivel_educativo = p.nivel_educativo
			obj.comunidad = p.comunidad
			obj.comunidad_finca = p.comunidad_finca
			obj.direccion_finca = p.direccion_finca
			obj.distancia_cabezera_departamental = p.distancia_cabezera_departamental
			obj.estado = p.estado
			obj.tipo = p.tipo
			# obj.id_tecnico = p.id_tecnico
			obj.latitud = p.latitud
			obj.longitud = p.longitud
			obj.save()

			p.update = True
			p.save()

	if prod_nuevos:
		query = prod_nuevos
		for x in query:	
			new_prod = ProductoresGeneral(id_fa = x.id,
										  nombre = x.nombre,
										  cedula = x.cedula,
										  sexo = x.sexo,
										  telefono = x.telefono,
										  nivel_educativo = x.nivel_educativo,
										  comunidad = x.comunidad,
										  comunidad_finca = x.comunidad_finca,
										  direccion_finca = x.direccion_finca,
										  distancia_cabezera_departamental = x.distancia_cabezera_departamental,
										  estado = x.estado,
										  tipo = x.tipo,
										  # id_tecnico = x.id_tecnico,
										  latitud = x.latitud,
										  longitud = x.longitud)
			new_prod.save(force_insert=True)
			x.update = True
			x.save()

	new = prod_nuevos.count()
	update = update_prod.count()

	return new,update

class CallbackTaskAldea(Task):
	def on_success(self, retval, task_id, args, kwargs):
		subject, from_email = 'Actualización BD Productores Aldea', 'no.reply.aldea@mail.com'
		text_content =  render_to_string('email/actualizacion_bd.txt', {'retval': retval,})

		html_content = render_to_string('email/actualizacion_bd.txt', {'retval': retval,})

		superusers_emails = User.objects.filter(is_superuser=True).values_list('email',flat=True)

		msg = EmailMultiAlternatives(subject, text_content, from_email, superusers_emails)
		msg.attach_alternative(html_content, "text/html")
		msg.send()

@shared_task(default_retry_delay=10, max_retries=3,base=CallbackTaskAldea)
def update_productores_aldea():
	prod_nuevos = ProductoresAldea.objects.filter(update = False).exclude(id__in = ProductoresGeneral.objects.exclude(id_aldea__isnull=True).values_list('id_aldea',flat=True))

	update_prod = ProductoresGeneral.objects.filter(Q(id_aldea__in = ProductoresAldea.objects.filter(update = False).values_list('id',flat=True)) |
													Q(cedula__in = ProductoresAldea.objects.filter(update = False).values_list('cedula',flat=True)))
	
	if update_prod:
		for obj in update_prod:
			p = ProductoresAldea.objects.get(Q(id = obj.id_aldea) | Q(cedula = obj.cedula))
			obj.id_aldea = p.id
			obj.nombre = p.nombre
			obj.cedula = p.cedula
			obj.sexo = p.sexo
			obj.telefono = p.telefono
			obj.nivel_educativo = p.nivel_educativo
			obj.comunidad = p.comunidad
			obj.comunidad_finca = p.comunidad_finca
			obj.direccion_finca = p.direccion_finca
			obj.distancia_cabezera_departamental = p.distancia_cabezera_departamental
			obj.estado = p.estado
			obj.tipo = p.tipo
			# obj.id_tecnico = p.id_tecnico
			obj.latitud = p.latitud
			obj.longitud = p.longitud
			obj.save()

			p.update = True
			p.save()

	if prod_nuevos:
		query = prod_nuevos
		for x in query:	
			new_prod = ProductoresGeneral(id_aldea = x.id,
										  nombre = x.nombre,
										  cedula = x.cedula,
										  sexo = x.sexo,
										  telefono = x.telefono,
										  nivel_educativo = x.nivel_educativo,
										  comunidad = x.comunidad,
										  comunidad_finca = x.comunidad_finca,
										  direccion_finca = x.direccion_finca,
										  distancia_cabezera_departamental = x.distancia_cabezera_departamental,
										  estado = x.estado,
										  tipo = x.tipo,
										  # id_tecnico = x.id_tecnico,
										  latitud = x.latitud,
										  longitud = x.longitud)
			new_prod.save(force_insert=True)
			x.update = True
			x.save()

	new = prod_nuevos.count()
	update = update_prod.count()

	return new,update

